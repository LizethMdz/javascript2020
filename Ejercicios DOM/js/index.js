const $menu = document.getElementById("menu__id");
const $btn_menu = document.getElementById("btn_menu");
let activado = true;

const flujoEventos = function (menu) {
    if (activado) {
        // Ocultar
        activado = false;
        menu.classList.add("menu__hidden");
    } else {
        // Mostrar
        activado = true;
        menu.classList.remove("menu__hidden");
    }
}

$btn_menu.addEventListener("click", (e) => {
    flujoEventos($menu);
});




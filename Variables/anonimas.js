/**Funcion anonima */

//Clasica

(function (){
    console.log('funcion autoejecutable clasica');
})();


(function (d, w, c){
    console.log(d);
})(document, window, console);

//Version Crockford
((function (){
    console.log('funcion crockford');
})());

//Unaria
+function (){
    console.log('funcion unaria');
}();

//Facebook
!function (){
    console.log('funcion facebook');
}();

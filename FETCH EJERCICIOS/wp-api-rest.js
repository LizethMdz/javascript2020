const d = document,
    w = window,
    $site = d.getElementById("site"),
    $posts = d.getElementById("posts"),
    $loader = d.querySelector(".loader"),
    $template = d.getElementById("post-template").content,
    $fragment = d.createDocumentFragment(),
    DOMAIN = "https://malvestida.com",
    SITE = `${DOMAIN}/wp-json`,
    API = `${SITE}/wp/v2`,
    POSTS = `${API}/posts?_embed`,
    PAGES = `${API}/pages`;

let page = 1,
    posts = 5;


function getSideData() {
    fetch(SITE)
        .then(res => res.ok ? res.json() : Promise.reject(res))
        .then(json => {
            //console.log(json);
            $site.innerHTML = `
                <h3>Sitio Web</h3>
                <h2>
                    <a href="${json.url}" target="_blank">${json.name}</a>
                </h2>
                <p>${json.description}</p>
                <p>${json.timezone_string}</p>
            `;
        })
        .catch(err => {
            let message = err.statusText || "Ocurrió un error con el API WORDPRESS";
            $site.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
            $loader.style.display = "none";
        });
}

function getPost() {
    $loader.style.display = "block";
    fetch(`${POSTS}&page=${page}&per_page=${posts}`)
        .then(res => res.ok ? res.json() : Promise.reject(res))
        .then(json => {
            //console.log(json);



            json.forEach(el => {
                let categorias = "", tags = "";

                el._embedded["wp:term"][0].forEach(el => categorias += `<li>${el.name}</li>`);
                el._embedded["wp:term"][1].forEach(el => tags += `<li>${el.name}</li>`);
                //console.log(el);
                $template.querySelector(".post-title").innerHTML = el.title.rendered;
                $template.querySelector(".post-image").src = el._embedded["wp:featuredmedia"] ? el._embedded["wp:featuredmedia"][0]["source_url"] : "";
                $template.querySelector(".post-image").alt = el.title.rendered;
                $template.querySelector(".post-author").innerHTML = `
                <img class="" src="${el._embedded["author"][0]["avatar_urls"][48]}" alt="${el._embedded["author"][0]["name"]}">
                <figcaption>${el._embedded["author"][0]["name"]}</figcaption>
                `;
                $template.querySelector(".post-date").textContent = new Date(el.date).toLocaleDateString();
                $template.querySelector(".post-link").href = el.link;
                $template.querySelector(".post-excerpt").innerHTML = el.excerpt.rendered.replace("[&hellip;]", "...");
                $template.querySelector(".post-categories").innerHTML = `
                    <p>Categorias</p>
                    <ul>${categorias}</ul>
                `;
                $template.querySelector(".post-tags").innerHTML = `
                <p>Etiquetas</p>
                    <ul>${tags}</ul>
                `;

                $template.querySelector(".post-content > article").innerHTML = el.content.rendered;
                let $clone = d.importNode($template, true);
                $fragment.appendChild($clone);
            });
            $loader.style.display = "none";
            $posts.appendChild($fragment);
        })
        .catch(err => {
            //console.log(err);
            let message = err.statusText || "Ocurrió un error con el API WORDPRESS";
            $posts.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
            $loader.style.display = "none";
        });

}

d.addEventListener("DOMContentLoaded", e => {
    getSideData();
    getPost();
});

w.addEventListener("scroll", (e) => {
    // scrollHeight - Total del scroll, clientHeight - Altura de la ventana
    const { scrollTop, clientHeight, scrollHeight } = d.documentElement;
    if (Math.ceil(scrollTop + clientHeight) >= scrollHeight) {
        //console.log("cargar mas...");
        page++;
        getPost();
    }
});
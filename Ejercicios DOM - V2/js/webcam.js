const d = document,
    n = navigator
export function webcam(id) {
    const $video = d.getElementById(id);
    if (n.mediaDevices) {
        const constraints = { audio: true, video: { width: 1280, height: 720 } };
        n.mediaDevices.getUserMedia(constraints)
            .then(function (mediaStream) {
                $video.srcObject = mediaStream;
                $video.onloadedmetadata = function (e) {
                    $video.play();
                };
            })
            .catch((err) => {
                $video.insertAdjacentHTML("beforeBegin", `<p><mark>${err}</mark></p>`);
                console.log(err.name + ": " + err.message);
            }); // always check for errors at the end.
    }
}
const btn21 = document.getElementById('btn_21');
const btn22 = document.getElementById('btn_22');
const btn23 = document.getElementById('btn_23');

btn21.addEventListener('click', () => {
    let string = prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion'); 
    elevarCuadrado(string);
});
btn22.addEventListener('click', () => {
    let string = prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion');
    mayorMenor(string);
});
btn23.addEventListener('click', () => {
    let string =prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion');
    paresImpares(string);
});

function elevarCuadrado(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let tmpIndex = null;
    let arrResultado = [];
    for (let index = 0; index < arrNumerico.length; index++) {
        tmpIndex = Math.pow(parseInt(arrNumerico[index]), 2);
        arrResultado.push(tmpIndex);
    }
    return alert('El arreglo final es: ' + arrResultado);
}

function mayorMenor(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let tmpIndex = null;
    let arrResultado = [];
    let mayor = parseInt(arrNumerico[0]) , menor = parseInt(arrNumerico[0]);
    for (let index = 0; index < arrNumerico.length; index++) {
        tmpIndex = parseInt(arrNumerico[index]);
        if (tmpIndex > mayor) {
            mayor = tmpIndex;
        }
        if(tmpIndex < menor){
            menor = tmpIndex;
        }
    }

    arrResultado.push(mayor,menor);
    return alert('El arreglo final es: ' + arrResultado);
}

function paresImpares(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let tmpIndex = null;
    let impares = [];
    let pares = [];
    for (let index = 0; index < arrNumerico.length; index++) {
        tmpIndex = parseInt(arrNumerico[index]);
        if (tmpIndex % 2 === 0) {
            pares.push(tmpIndex);
        } else {
            impares.push(tmpIndex);
        }
    }
    let obj = {
        pares,
        impares
    }

    return alert('El arreglo final es: ' + obj.impares + '---' + obj.pares);
}
//Ambito global
var hola = "Hola mundo";
//Ambito de bloque
let hello = "Hello Word";
console.log(hola);

console.log("****var*****");

var musica = "rock";
console.log(musica);

{
    var musica = "pop";
    console.log(musica);

}

console.log(musica);
console.log("****let*****");

let musica2 = "rock";
console.log(musica2);

{
    let musica2 = "pop";
    console.log(musica2);

}

console.log(musica);
console.log(musica2);

console.log("****const*****");

const PI = 3.1416;

console.log("****objetos*****");

let colores = ["black", "white", "blue", "red"];

let figura = {
    nombre : "cuadrado",
    ancho: 30,
    largo: 45
}

console.log(colores);
console.log(figura);

/**Cuando hay un valor primitivo los datos no pueden ser modificados */
/**Cuando es un dato compuesto, no hay problema, porque en si la referencia no cambia */

/**Una variable declarada como null tiene una referencia y un valor especial */
/**Una variable undefined esta declarada pero no inicilizada */

let a;
let b = null;

/** Funciones */

function saludar(nombre, edad){
    console.log(`Hola mi nombre es ${nombre} y tengo ${edad} años.`);
}

saludar('lizeth', 23);

/**Funcion anonima 
 * si se mandara llamar antes, mostrara un error
*/
const funcionExpresada = () => {
    console.log('Funcion anonima');
}

funcionExpresada();

/***For in  */
/**Me ayuda a recorrer las prpiedades de un objeto */

const lizeth = {
    nombre: 'liz',
    edad: 15,
    apellido: 'mdz'
}

for (const key in lizeth) {
    if (lizeth.hasOwnProperty(key)) {
        const element = lizeth[key];
        const llave = key;
        console.log(llave);
        console.log(element);
        
    }
}

let colors = ['rojo', 'azul', 'negro', 'rosa'];

for (const key of colors) {
    console.log(key);
}

let frase = "Gutten morgen";

console.log(frase.includes('t'));

let contador = 0;
for (let index = 0; index < frase.length; index++) {
    const element = frase[index];
    console.log('Valor: ' + element);
    if (element === "t"){
        contador++;
    }
    
}

console.log('El numero total de letras t es:' + contador);

/**Objetos y arreglos con desestructuracion */
let casa = [1,2,3];
let pintura = casa[0], pizos = casa[1], recamaras = casa[2];

let [pintura2, pizos2, recamaras2] = casa;

console.log('Propiedad sin estructurar: ' + pintura2);
let persona = {
    nombre: 'luis',
    edad : 25,
    apellido : 'lopez'
}

let {nombre, edad, apellido} = persona;

let persona2 = Object.create(persona);

console.log(persona);
console.log('Propiedad sin estructurar: ' + nombre);
console.log(persona2.nombre);

/** Objetos literales */
let raza = 'labrador';


let perro = {
    nombre: 'flays',
    color: 'cafe',
    edad: 2,
    raza : 'pitbull',
    ladrar () {
        console.log('wau wau!!');
    }
}
/**Si existe una propiedad, automaticamente actua como una propiedad y su valor */

let dog = {
    nombre: 'flays',
    color: 'cafe',
    edad: 2,
    raza,
    ladrar () {
        console.log('wau wau!!');
    }
}

console.log(perro);
console.log(dog);

/**Parametros REST & Operador spred */

function sumar(a,b, ...c){
    let res = a+ b;

    c.forEach((n) => {
        res += n;
    })
    return res;
} 

console.log(sumar(1,2));
console.log(sumar(1,2, 5));

console.log(sumar(1,2, 2,8,7,8));

let numeros = [1,2,3,4,5], num = [6,7,8,9,0];

let integracion = [...num, ...numeros];

console.log(integracion);

/**Arrow Function */

let restar =  (a, b) => a - b;

numeros.forEach( (val, ind) => {
    console.log(`Valor: ${val} y Indice: ${ind}`);
})


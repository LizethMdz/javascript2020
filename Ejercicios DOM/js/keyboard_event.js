const $ball = document.querySelector("#ball");
const $canvas = document.querySelector(".canvas__ball");
const d = document;
let posX = 0, posY = 0;


const tipoAlerta = (mensaje, key, e) => {
    e.preventDefault();
    switch (key) {
        case 'a':
            alert(mensaje);
            break
        case 'c':
            confirm(mensaje);
            break
        case 'p':
            prompt(mensaje);
            break
        default:
            console.warn('No es una opcion valida');

    }
}
// Obtiene los datos del DIV
const limCanvas = $canvas.getBoundingClientRect();
const limPelotas = $ball.getBoundingClientRect();

console.log(limCanvas);
console.log(limPelotas);

const moverPelota = (direccion) => {
    if (posX < 750) {
        if (direccion === 'right') {
            posX += 10;
        }
    }

    if (posX >= 40) {
        if (direccion === 'left') {
            posX -= 10;
        }
    }

    if (posY >= 40) {
        if (direccion === 'up') {
            posY -= 10;
        }
    }

    if (posY <= 340) {
        if (direccion === 'down') {
            posY += 10;
        }
    }
    $ball.style.position = 'absolute';
    // $ball.style.left = `${posX}px`;
    // $ball.style.top = `${posY}px`;
    $ball.style.transform = `translate(${posX}px, ${posY}px)`
}


d.addEventListener("keydown", (e) => {
    if (e.key === 'a' && e.altKey) {
        tipoAlerta('Soy una alert', e.key, e);
    }

    if (e.key === 'c' && e.altKey) {
        tipoAlerta('Soy un confirm', e.key, e);
    }

    if (e.key === 'p' && e.altKey) {
        tipoAlerta('Soy un promt', e.key, e);
    }

    if (e.key === 'ArrowRight') {
        moverPelota('right');
    } else if (e.key === 'ArrowLeft') {
        moverPelota('left');
    } else if (e.key === 'ArrowDown') {
        moverPelota('down');
    } else if (e.key === 'ArrowUp') {
        moverPelota('up');
    }

});





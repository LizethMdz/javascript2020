const d = document,
    $form = d.getElementById("song-search"),
    $loader = d.querySelector(".loader"),
    $error = d.querySelector(".error"),
    $main = d.querySelector("main"),
    $artist = d.querySelector(".artist"),
    $song = d.querySelector(".song");

// URL https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search
// URL https://www.theaudiodb.com/api_guide.php

d.addEventListener("submit", async (e) => {
    if (e.target === $form) {
        e.preventDefault();
        try {
            $loader.style.display = "block";
            let artist = e.target.artist.value.toLowerCase(),
                song = e.target.song.value.toLowerCase(),
                $artistTemplate = "",
                $songTemplate = "",
                artistAPI = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artist}`,
                songAPI = `https://api.lyrics.ovh/v1/${artist}/${song}`,
                artistF = fetch(artistAPI),
                songF = fetch(songAPI),
                [artistR, songR] = await Promise.all([artistF, songF]),
                artistData = await artistR.json(),
                songData = await songR.json();
            console.log(artistData, songData);

            if (artistData.artist === null) {
                $artistTemplate = `<h2>No existe el intérprete <mark>${artist}</mark></h2>`
            } else {
                let artistFinal = artistData.artists[0];
                $artistTemplate = `
                        <h2>${artistFinal.strArtist}</h2>
                        <img src="${artistFinal.strArtistThumb}" alt="${artistFinal.strArtist}" />
                        <p>${artistFinal.intBornYear} - ${(artistFinal.intDiedYear || "Presente")}</p>
                        <p>${artistFinal.strCountry}</p>
                        <p>${artistFinal.strGenre} - ${artistFinal.strStyle}</p>
                        <a href="://${artistFinal.strWebsite}" target="_blank">Sitio Web</a>
                        <p>${artistFinal.strBiographyEN}</p>
                    `;
            }

            if (songData.lyrics === "") {
                $songTemplate = `<h2>No existe la canción  <mark>${song}</mark></h2>`
            } else {
                $songTemplate = `
                    <h2>${song.toUpperCase()}</h2>
                    <blockquote>${songData.lyrics}</blockquote>
                `;
            }
            $loader.style.display = "none";
            $artist.innerHTML = $artistTemplate;
            $song.innerHTML = $songTemplate;



        } catch (err) {
            $loader.style.display = "none";
            let message = err.statusText || "Ocurrió un error con el API LYRICS";
            $error.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
        }
    }
});
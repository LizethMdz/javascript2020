# INTRODUCCIÓN

Capaz de ejecutar 3 capas de una aplicación:

1. FRONTEND
1. BACKEND
1. PERSISTENCIA DE DATOS(MONGO, COUCH DB, FIREBASE, ETC.)

Con javascript puedes:

- Diseño y desarrollo Web
- Hacer videojuegos
- Experiencia 3D, Realidad Aumentada y Virtual
- Controlar Hardware
- Aplicaciones Hibridas
- Machine Learning

Caracteristicas:

- Lenguaje de alto nivel
- Interpretado
- Dinamico
- Debilmente Tipado
- Multiparadigma
- Sensible a mayusculas y mimusculas
- No necesitas los puntos y comas y comas al fnilas de cada linea.

### Escritura de codigo

- Los identificadores deben comenzar con:
  - Una letra o
  - Un signo de dolar \$ o
  - Un guion bajo \_
  - Sin numeros
- Usa snake_case en:
  - Archivos
- Usa UPPER_CASE en:
  - Constantes
- Usa UpperCamelCase en:
  - Clases
- Usa lowerCamelCase en:
  - Objetos
  - Primitivos
  - Funciones
  - Instancias

### Ordenamiento de Codigo

1. Importacion de modulos
1. Declaracion de variables
1. Declaracion de Funciones
1. Ejecucion de Codigo

### Tipos de datos en Java Script

1. Primitivos: se accede directamente al valor
   1. string
   1. number
   1. boolean
   1. null
   1. undefined
   1. NaN
   1. Symbols
1. Compuestos: Se accede a la referencia del valor
   1. Object = {}
   1. array = []
   1. function (){}
   1. Class {}
   1. Etc.

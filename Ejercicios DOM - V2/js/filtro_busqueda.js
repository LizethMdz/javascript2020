const d = document;

export function filter(input, selector) {
    d.addEventListener("keyup", (e) => {
        if (e.target.matches(input)) {
            const palabra = e.target.value;
            const cards = d.querySelectorAll(selector);
            cards.forEach((el) => {
                if (el.textContent.toLowerCase().includes(palabra)) {
                    el.classList.remove("filter");
                } else {
                    el.classList.add("filter");
                }
            });
        }
    });

}
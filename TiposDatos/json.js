/**JSON JavaScript Object Notation */

// 

const persona = {
    nombre: "lizeth",
    edad: 23,
    redes: {
        email: "liz@mail.com",
        twitter: "@LizethMdzT"
    }
}

console.log(persona);

console.log(JSON);

console.log(JSON.parse("[1,2,6,54]"));

console.log(JSON.parse("null"));

console.log(JSON.parse("19"));

console.log(JSON.parse("true"));

// No puede parse() una cadena 
// console.log(JSON.parse("hola"));

// No puede parse() undefined 
// console.log(JSON.parse("undefined"));



console.log(JSON.stringify([1, 2, 6, 54]));

console.log(JSON.stringify(null));

console.log(JSON.stringify(19));

console.log(JSON.stringify(true));

console.log(JSON.stringify({ x: 1, y: 3 }));

console.log(JSON.stringify(persona));
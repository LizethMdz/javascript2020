class Animal {
    
    constructor(nombre, genero){
        this.nombre = nombre;
        this.genero = genero;
    }

    saludar () {
        console.log('Me llamo: ' + this.nombre);
    }
}
class Perro extends Animal {

    constructor(nombre, genero, tamanio){
        super(nombre, genero);
        this.tamanio = tamanio;
        this.raza = null;
    }

    get getTamanio(){
        return this.tamanio;
    }

    set setTamanio(tamanio){
        this.tamanio = tamanio;
    }
    
    get getRaza(){
        return this.raza;
    }

    set setRaza(raza){
        this.raza = raza;
    }

    saludar() {
        console.log('wau wau!!');
    }

    gruñir(){
        console.log('rrrrrrr rrrrrr!');
    }

    //Metodo estatico
    static queEs(){
        console.log("Somo perros!!");
    }
}

let scooby = new Perro('Scooby', 'Macho', 'Mediano');

scooby.gruñir();
scooby.saludar();

Perro.queEs();
console.log(scooby);
console.log(scooby.getRaza);
scooby.setRaza = "Labrador";
console.log(scooby.getRaza);
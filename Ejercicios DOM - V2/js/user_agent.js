const d = document,
    n = navigator,
    ua = n.userAgent;

export function userAgent(id) {
    const $info_device = d.getElementById("user-device");
    const isMobile = {
        android: () => ua.match(/android/i),
        ios: () => ua.match(/ios/i),
        windows: () => ua.match(/windows phone/i),
        any: function () {
            return this.android() || this.ios() || this.windows();
        }
    }

    const isDesktop = {
        linux: () => ua.match(/linux/i),
        mac: () => ua.match(/mac/i),
        windows: () => ua.match(/windows nt/i),
        any: function () {
            return this.linux() || this.mac() || this.windows();
        }
    }

    const isBrowser = {
        firefox: () => ua.match(/firefox/i),
        chrome: () => ua.match(/chrome/i),
        opera: () => ua.match(/opera/i),
        edge: () => ua.match(/edge/i),
        ie: () => ua.match(/trident/i),
        any: function () {
            return (
                this.firefox() ||
                this.chrome() ||
                this.opera() ||
                this.edge() ||
                this.ie()
            );
        }
    };

    $info_device.innerHTML = /*html*/ `
    <ul>
        <li>${ua}</li>
        <li>Platform: ${isMobile.any() ? isMobile.any() : isDesktop.any()}</li>
        <li>Web Browser: ${isBrowser.any()}</li>
    </ul>
    `;

    // Contenido exclusivo para navegadores
    if (isBrowser.chrome()) {
        $info_device.innerHTML += "Estas en Chrome :)";
    }

    if (isBrowser.edge()) {
        $info_device.innerHTML += "Estas en Edge :D";
    }

    if (isBrowser.firefox()) {
        $info_device.innerHTML += "Estas en Firefox :D";
    }
}
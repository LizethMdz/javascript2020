// - Nombre: ^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$
// - Correo: ^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$
// - Comentarios: ^.{1,255}$

// También el link de los loaders en SVG: https://samherbert.net/svg-loaders/
const d = document;

export function contactFormValidations() {
    const $form = d.querySelector(".contact-form");
    const $inputs = d.querySelectorAll(".contact-form [required]");

    $inputs.forEach((input) => {
        const $span = d.createElement("span");
        $span.id = input.name;
        $span.textContent = input.title;
        $span.classList.add("contact-form-error", "none");
        input.insertAdjacentElement("afterend", $span);
    });

    d.addEventListener("keyup", (e) => {
        if (e.target.matches(".contact-form [required]")) {
            let $input = e.target, pattern = $input.pattern || $input.dataset.pattern;
            // console.log($input, pattern);

            // Validaciones de patrones
            if (pattern) {
                let regexp = new RegExp(pattern);
                return !regexp.exec($input.value) ? d.getElementById($input.name).classList.add("is-active")
                    : d.getElementById($input.name).classList.remove("is-active")
            }

            if (!pattern) {
                return $input.value === "" ? d.getElementById($input.name).classList.add("is-active")
                    : d.getElementById($input.name).classList.remove("is-active")
            }
        }
    });
    d.addEventListener("submit", (e) => {
        // e.preventDefault();
        alert("Enviando formulario");
        const $loader = d.querySelector(".contact-form-loader"),
            $response = d.querySelector(".contact-form-response");

        $loader.classList.remove("none");

        setTimeout(() => {
            $loader.classList.add("none");
            $response.classList.remove("none");
            $form.reset();

            setTimeout(() => {
                $response.classList.add("none");
            }, 3000);
        }, 3000);
    });
}
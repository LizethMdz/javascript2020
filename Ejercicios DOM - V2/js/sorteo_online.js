const d = document;

const lenguajes = [
    "PHP",
    "JAVA",
    "GO",
    "PYTHON",
    "C++",
    "C#",
    "RUBY",
    "COBOL",
    "JAVASCRIPT",
    "SQL"
];

export function sorteoOnline(id, btn_id) {
    const $ln = d.querySelector(id);
    const $ul = d.createElement("ul");
    const $fragment = d.createDocumentFragment();

    lenguajes.forEach((el) => {
        const $li = d.createElement("li");
        $li.insertAdjacentText("afterbegin", el);
        $fragment.appendChild($li);
    });

    $ul.appendChild($fragment);
    $ln.appendChild($ul);

    const ganador = () => {
        const random = Math.floor(Math.random() * lenguajes.length);
        const winner = lenguajes[random];

        return `El ganador es ${winner}`;
    }
    d.addEventListener("click", (e) => {
        if (e.target.matches(btn_id)) {
            alert('El ganador es ' + ganador());
        }
    });

}


export function sorteoOnline_v2(id, selector) {
    const ganador_v2 = (selector) => {
        const $players = d.querySelectorAll(selector);
        const random = Math.floor(Math.random() * $players.length);
        const winner = $players[random];

        console.log(winner);

        return `El ganador es ${winner.textContent}`;
    }

    d.addEventListener("click", (e) => {
        if (e.target.matches(id)) {
            alert('El ganador es ' + ganador_v2(selector));
        }
    });
}
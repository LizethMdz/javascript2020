const btn12 = document.getElementById('btn_12');
const btn13 = document.getElementById('btn_13');
const btn14 = document.getElementById('btn_14');

btn12.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un numero'));
    esPrimo(valor);
});
btn13.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un numero'));
    esImparPar(valor);
});
btn14.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un valor para calcular la temperatura en °C o °F'));
    let opcion = prompt('Ingrese la opcion C(Celsius) o F(Farenheit) segun sea el caso');
    gradosCelsius(valor, opcion);
});

function esPrimo(valor){
    if(!valor || typeof valor === "string"){
        alert('Introduce un numero');
    }else if(valor < 1 || Math.sign(valor) === -1){
        alert('Introduce un numero mayor a uno por convencion!!');
    }else{
        let x = 1;
        let cont = 0;
        while(x <= valor){
            if(valor % x === 0){
                cont++;
            }
            x++;
        }
        if(cont === 2){
            return alert('Es un numero primo');
        }else{
            return alert('No es un numero primo');
        }

        
    }
}

function esImparPar(valor){
    if(!valor || typeof valor === "string"){
        alert('Introduce un numero');
    }else if(valor < 1 || Math.sign(valor) === -1){
        alert('Introduce un numero mayor a uno por convencion!!');
    }else{
        if(valor % 2 !== 0){
            return alert('Es un numero impar');
        }else{
            return alert('Es un numero par');
        }
    }
}

function gradosCelsius(valor, opcion){
    const multi = 1.8;
    let validado = validarEntrada(valor);
    opcion = opcion.toUpperCase();
    if(typeof valor === "string" || !validado){
        alert('No colocaste un numero valido');
    }else if(opcion === 'C' && valor >= 0){
        return alert(((valor * multi) + 32) + "°F");
    }else if(opcion === 'F' && valor >= 0){
        return alert(((valor - 32) / multi) + "°C");
    }else{
        return alert('No ingreso una opcion valida');
    }
}


function validarEntrada(cadena){
    let validado = parseInt(cadena);
    if (isNaN(validado)){
        return false;
    }if( typeof validado === 'number') {
        return true;
    }else{
        return false;
    }
}
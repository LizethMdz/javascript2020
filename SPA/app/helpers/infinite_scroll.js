import api from "../helpers/wp_api.js";
import { ajax } from "../helpers/ajax.js";
import { SearchCard } from "../components/SearchCard.js";
import { PostCard } from "../components/PostCard.js";
export const InfiniteScroll = async () => {
    const d = document,
        w = window;

    let query = localStorage.getItem("wpSearch"),
        apiUrl,
        Component; //HIGH ORDER COMPONENT

    w.addEventListener("scroll", async (e) => {
        let { scrollTop, clientHeight, scrollHeight } = d.documentElement;
        let { hash } = w.location;

        //console.log(scrollTop, clientHeight, scrollHeight, hash);

        if (scrollTop + clientHeight >= scrollHeight) {
            api.page++;
            if (!hash || hash === "#/") {
                apiUrl = `${api.POSTS}&page=${api.page}`;
                Component = PostCard;
            } else if (hash.includes("#/search")) {
                apiUrl = `${api.SEARCH}${query}&page=${api.page}`;
                Component = SearchCard;
            } else {
                return false;
            }

            d.querySelector(".loader").style.display = "block";

            await ajax({
                url: apiUrl,
                cbSuccess: (posts) => {
                    //console.log(posts);
                    let html = "";
                    posts.forEach(element => html += Component(element));
                    d.getElementById("main").insertAdjacentHTML("beforeend", html);
                    d.querySelector(".loader").style.display = "none";
                }
            })
        }
    });

}
const iterable = [1, 2, 3, 4, 5, 6, 2, 1, 2, 23, 2, 2, 9];

//Propiedad especial de los elementos que sean iterables
const iterator = iterable[Symbol.iterator]();

console.log(iterable);

console.log(iterator);
let next = iterator.next();

while (!next.done) {
    console.log(next.value);
    next = iterator.next();
}
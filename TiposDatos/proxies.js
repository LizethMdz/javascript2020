const persona = {
    nombre: "",
    apellido: "",
    edad: 0
}

/**El manejador es importante debido a que es el manejador */
const manejador = {
    set(obj, prop, valor) {
        // Para evaluar las propiedades que existen en el objeto
        if (Object.keys(obj).indexOf(prop) === -1) {
            // Marcara este error debido a que restringimos la existencia de nuevas propiedades
            return console.error(`La propiedad ${prop} no existe en la variable persona`);
        }

        if ((prop === "nombre" || prop === "apellido") && !(/^[A-Za-zÑnÁáÉéÍíÓóÚú\s]+$/g.test(valor))) {
            return console.error(`La propiedad ${prop} solo acepta letras y espacios en blanco`);
        }
        obj[prop] = valor;
    }
}

const liz = new Proxy(persona, manejador);
console.log(liz);
liz.nombre = "Liz";
// Generara un error por los valores aceptados 
liz.apellido = "Mdz---";
liz.edad = 23;

liz.twitter = "@LizethMdzT";

console.log(liz);

// Mantiene vinculado la forma del objeto
console.log(persona);

// Pero tambien se puede establecer que e objeto no cambio desde el original
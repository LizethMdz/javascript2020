const d = document,
    n = navigator;

export function geolocalization() {
    const status = d.querySelector('#status');
    const data = d.querySelector('#data-status');
    const mapLink = d.querySelector('#map-link');

    const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
    }

    const success = (position) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        const accuracy = position.coords.accuracy;

        status.textContent = '';
        data.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °, Accuracy ${accuracy}`;
        // https://www.google.com/maps/@20.5478648,-100.3899499,16z
        mapLink.href = `https://www.google.com/maps/@${latitude},${longitude},16z`;
        mapLink.textContent = "Ver mapa";
        //mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
    }

    const error = () => {
        status.innerHTML = '<p><mark>Unable to retrieve your location</mark></p>';
    }

    if (!navigator.geolocation) {
        status.innerHTML = '<p><mark>Geolocation is not supported by your browser</mark></p>';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error, options);
    }
}
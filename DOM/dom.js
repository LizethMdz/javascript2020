/** Objeto global */
// console.log(window);
// Funcion que dicta una voz 

// let texto = "Hola, soy lizeth";

// const hablar = (texto) => {
//     speechSynthesis.speak(new SpeechSynthesisUtterance(texto));

// }

//hablar(texto);

/** Elementos del docuemnto */

// console.log(document);
// console.log(document.body);
// console.log(document.head);
// console.log(document.doctype);
// console.log(document.characterSet);
// console.log(document.scripts);
// console.log(document.forms);
// console.log(document.styleSheets);

/**Nodos, elementos y selectores */

console.log(document.getElementsByTagName("li"));

console.log(document.getElementsByClassName("card"));

console.log(document.getElementsByName("nombre"));

console.log(document.getElementById("menu"));

console.log(document.querySelector("#menu"));

// Solo trae el primer elemento encontrado
console.log(document.querySelector("a"));

// Devuelve la coleccion de enlaces
console.log(document.querySelectorAll("a"));
// Devuelve la coleccion de enlaces y su valor
document.querySelectorAll("a").forEach(el => console.log(el));

// Devuelve el elemento en esa posicion
console.log(document.querySelectorAll(".card")[2]);

// Devuelve los elementos dentro de #menu
console.log(document.querySelectorAll("#menu li"));

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/**Atributos Y data-atributos */

console.log(document.documentElement.lang);

console.log(document.documentElement.getAttribute("lang"));

// Como acceder a losatributos

console.log(document.querySelector(".link__dom").href);

console.log(document.querySelector(".link__dom").getAttribute("href"));

// Establecer nuevos colores a los atributos

document.documentElement.lang = "es";

document.documentElement.setAttribute("lang", "es-MX");

console.log(document.documentElement.lang);

// Todas las variables con elementos del DOM, 
// es buena practica anteponer 
// $
const $linkDom = document.querySelector('.link__dom');

$linkDom.setAttribute("target", "_blank");
$linkDom.setAttribute("href", "www.google.com");
$linkDom.setAttribute("rel", "noopener");

// Conprombar la existencia de un atributo
console.log($linkDom.hasAttribute("rel"));

// Data Attribute

console.log($linkDom.getAttribute("data-description"));

// Accede a todo los "data" attributes 
console.log($linkDom.dataset);

$linkDom.setAttribute("data-description", "Modelo de Objeto del documento");

// Notacion con puntos

$linkDom.dataset.description = "JAVASCRIPT DEVELOPMENT";
console.log($linkDom.dataset.description);

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/**Estilos y variables */

// Propiedades con Camel Case
console.log($linkDom.style);
console.log($linkDom.getAttribute("style"));
console.log($linkDom.style.color)

// Consola de propiedades computadas
console.log(window.getComputedStyle($linkDom));


console.log(window.getComputedStyle($linkDom).getPropertyValue("color"));

// Modificar una propiedad
// Con el nombre igual que en css
$linkDom.style.setProperty("text-decoration", "none");
$linkDom.style.setProperty("display", "block");
$linkDom.style.width = "50%";
$linkDom.style.textAlign = "center";
$linkDom.style.marginLeft = "auto";
$linkDom.style.marginRight = "auto";
$linkDom.style.padding = "1rem";
$linkDom.style.borderRadius = ".5rem";
$linkDom.style.backgroundColor = "#f7df1e";

document.body.style.margin = 0;

// Acceder a las variables CSS
const $html = document.documentElement,
    $body = document.body;

let varDark = getComputedStyle($html).getPropertyValue("--dark-color");
let varYellow = getComputedStyle($html).getPropertyValue("--yellow-color");

console.log(varDark, varYellow);

$body.style.backgroundColor = varDark;
$body.style.color = varYellow;

// Solo establezco el valor de color rosa, pero aun no tiene efectos
$html.style.setProperty("--dark-color", "#000000");

varDark = getComputedStyle($html).getPropertyValue("--dark-color");

//$body.style.setProperty("background-color", varDark);

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/**Agregar propiedades CSS en el HTML desde un archivo */

$card = document.querySelector(".card");

console.log($card);
// Regresa una cadena de las clases agregadas
console.log($card.className);
// Regresa una coleccion de clases del atributo class=""
console.log($card.classList);
// Validar una clase
console.log($card.classList.contains("rotate-45"));
// Para agregarla seria de la siguiente manera
$card.classList.add("rotate-45");
// Vuelvo a validar
console.log($card.classList.contains("rotate-45"));
// Quita una clase
$card.classList.remove("rotate-45");
// Evalua si la tiene? quitar : agregar
$card.classList.toggle("rotate-45");
// Reemplazar una clase | Recibe la anterior y la nueva
$card.classList.replace("rotate-45", "rotate-135");

// Agregar mas clases a un elemnto
$card.classList.add("opacity-80", "sepia");
// Tambien sirve para el metodo remove


/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
*/

/**TEXTO Y HTML */

const $parrafo = document.getElementById("id");

let text = `
    <p>
        El modelo de objetos del documento(<b><i>DOM - Document Object Model</i></b>)
        es una API para documentos HTML y XML
    </p>
    <p>
        Este provee una representacion estructural del documento permitiendo modificar su contenido y 
        presentacion visual mediante codigo JS.
    </p>
    <p>
        <mark>
            El DOM no es parte de la especificaion de JavaScript es una API para los mnavegadores
        </mark>
    </p>
`;
// Propiedad no estandar
//$parrafo.innerText = text;
// Propiedad estandar - pero no respeta las propiedades de HTML
// Utilizar solo si es texto puro
$parrafo.textContent = text;
// Esta siagrega el texto con propiedades HTML respetadas
// Reempazar el contenido en el DOM
$parrafo.innerHTML = text;
// Realiza una reemplazo mayor, es decir cambia la estrcutra en el 
// HTML
$parrafo.outerHTML = text;

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/**Transversing : Recorriendo el DOM */

// Enfocado a las etiquetas del html, por lo que ignoramos los demas nodos

$cards = document.querySelector(".cards");

// Hacer referencia a los elementos hijos

console.log($cards.children);
// Hacer referencia a los nodos hijos con todas sus etiquetas
console.log($cards.childNodes);


console.log($cards.children[2]);
console.log($cards.parentElement);
console.log($cards.firstElementChild);
console.log($cards.lastElementChild);
console.log($cards.previousElementSibling);
console.log($cards.nextElementSibling);

console.log($cards.closest("nav"));
console.log($cards.closest("body"));
console.log($cards.children[3].closest("section"));

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/**Creando elementos y fragmentos */

/**
 * Creates a new empty DocumentFragment into which 
 * DOM nodes can be added to build an offscreen DOM tree.
 */

const $figure = document.createElement("figure");
const $img = document.createElement("img"),
    $figcaption = document.createElement("figcaption"),
    $figcaptionText = document.createTextNode("Animals"),
    $cards_2 = document.querySelector(".cards");

$img.setAttribute("src", "https://rockcontent.com/es/wp-content/uploads/2019/02/google-trends-1280x720.png");
$img.setAttribute("width", "200px");
$figure.classList.add("card");

$figcaption.appendChild($figcaptionText);
$figure.appendChild($img);
$figure.appendChild($figcaption);
// Agregar un elemento desde el padre
$cards_2.appendChild($figure);

// Forma rapida, pero tan usada
// Con el uso de innerHTML
// Se agregan las clases padres y ademas se agrega el elemento hijo

// Otra forma de agregar elementos dinamicamente
const estaciones = ['Primavera', 'Verano', 'Otoño', 'Invierno'];
const $ul = document.createElement("ul");
document.write("<h3>Estaciones del año</h3>");
document.body.appendChild($ul);

estaciones.forEach((season) => {
    const $li = document.createElement("li");
    $li.textContent = season;
    $ul.appendChild($li);
});

const continentes = ['Africa', 'America', 'Asia', 'Europa', 'Oceania'];
const $ul2 = document.createElement("ul");
document.write("<h3>Continentes del mundo</h3>");
document.body.appendChild($ul2);
// Se le agrega el innerHTML para inicializarlo, de lo contrario
// los valores seran reemplazados
$ul2.innerHTML = "";
// Cada iteraciones le pegamos al DOM, pero no es lo que buscamos,
// cuando la idea sea agregar una plantilla de elementos
continentes.forEach((c) => ($ul2.innerHTML += `<li>${c}</li>`));

// Para ello usamos la tecnica del Fragmento, es la mas optima
const meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
],
    $ul3 = document.createElement("ul"),
    $fragment = document.createDocumentFragment();


meses.forEach(m => {
    const $li = document.createElement("li");
    $li.textContent = m;
    $fragment.appendChild($li);
});
document.write("<h3>Meses del Año</h3>");
$ul3.appendChild($fragment);
document.body.appendChild($ul3);

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/** Templates */

/**
 * El elemento HTML <template> es un mecanismo 
 * para mantener el contenido HTML del lado del cliente que no se 
 * renderiza cuando se carga una página, pero que posteriormente 
 * puede ser instanciado durante el tiempo de ejecución 
 * empleando JavaScript.
 */

const $cards_3 = document.querySelector(".cards"),
    $template = document.getElementById("template-card").content,
    $fragmento = document.createDocumentFragment(),
    cardContent = [
        {
            title: "Tecnologia",
            img: "http://placeimg.com/640/480/tech"
        },
        {
            title: "Personas",
            img: "http://placeimg.com/640/480/people"
        },
        {
            title: "Arquitectura",
            img: "http://placeimg.com/640/480/arch"
        }
    ];

cardContent.forEach(el => {
    //Hacer referencia al elemento template
    $template.querySelector("img").setAttribute("src", el.img);
    $template.querySelector("img").setAttribute("alt", el.title);
    $template.querySelector("img").setAttribute("width", "200px");
    $template.querySelector("figcaption").textContent = el.title;

    // Pero al hacerlo asi, solamente generariamos uno, por lo que
    // se declara la creacion de un nodo clon
    let clone = document.importNode($template, true);
    $fragmento.appendChild(clone);
});

// Se anexa al elemento padre
$cards_3.appendChild($fragmento);

/**
 * 
 * 
 * 
 * 
 * 
 *
 * 
 * 
 * 
*/

/** Modificando elementos (Old Style) */

const $cards_4 = document.querySelector(".cards"),
    $newCard = document.createElement("figure"),
    $cloneCard = $cards_4.cloneNode(true);

$newCard.innerHTML = `
<img src="http://placeimg.com/640/480/any" width="200px2" />
<figcaption>Any</figcaption>
`;

$newCard.classList.add("card");

// Reemplazar en una posicion
//$cards_4.replaceChild($newCard, $cards_4.children[2]);
// Colocar antes de un elemento
//$cards_4.insertBefore($newCard, $cards_4.firstElementChild);
// Eliminar un elemento en especifico
//$cards_4.removeChild($cards_4.lastElementChild);

document.body.appendChild($cloneCard);


/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */


/** DOM: Modificando elementos (Cool Style) */

// insertAdjacent
// .insertAdjacentElement(position, el)
// .insertAdjacentHTML(position, html)
// .insertAdjacentText(position, text)

// Posiciones:
// beforebegin(hermano anterior)
// afterbegin(primier hijo)
// beforeend(ultimo hijo)
// afterend(hermano siguiente)

const $cards_5 = document.querySelector(".cards"),
    $newCard2 = document.createElement("figure");

let $contentCard = `
<img src="http://placeimg.com/640/480/any" width="200px2" />
<figcaption></figcaption>
`;

$newCard2.classList.add("card");

// Insertar texto dentro de un elemento
$newCard2.insertAdjacentHTML("beforeend", $contentCard);
$newCard2.querySelector("figcaption").insertAdjacentText("afterbegin", "Any");

// Se insertó con lo que tenia la variable $newCard2.innerHTML
$cards_5.insertAdjacentElement("afterbegin", $newCard2);

// Primer hijo
//$cards_5.prepend($newCard2)
// Ultimo hijo
//$cards_5.append($newCard2)
// Hermano anterior
//$cards_5.before($newCard2)
// Hermano posterior
//$cards_5.after($newCard2)
/**No son elementos iterables
 * No se pueden limpiar (clear())
 * No tienen propiedad size
 */

//No es valido

// const ws = new WeakSet([
//     1, 2, 3, 6, 8, 7, 1, 2, 3, true, "hola"
// ]);

// console.log(ws);

const ws = new WeakSet();

let valor1 = { "valor1": 1 };
let valor2 = { "valor2": 2 };
let valor3 = { "valor3": 3 };

ws.add(valor1);
ws.add(valor2);
ws.add(valor3);

console.log(ws);

// setInterval(() => console.log(ws), 1000);
// setTimeout(() => {
//     valor1 = null;
//     valor2 = null;
//     valor3 = null;
// }, 5000);

// Tampoco es valido
// const wmapa = new WeakMap([
//     ["nombre", "flay"],
//     ["edad", 5],
//     ["animal", "perro"],
//     [null, "nulo"]
// ]);

const wmapa = new WeakMap();

let llave1 = {};
let llave2 = {};
let llave3 = {};

wmapa.set(llave1, 1);

wmapa.set(llave2, 2);

wmapa.set(llave3, 3);

console.log(wmapa);
/** BOM: Propiedades eventos */

window.addEventListener("resize", (e) => {
    console.clear();
    console.log(window.innerWidth);
    console.log(window.innerHeight);


    console.log(window.outerWidth);
    console.log(window.outerHeight);

    console.log(e);

});

window.addEventListener("scroll", (e) => {
    console.clear();
    console.log(window.scrollX);
    console.log(window.scrollY);

    console.log(e);
});


window.addEventListener("load", (e) => {
    console.log(window.screenX);
    console.log(window.screenY);

    console.log(e);
});

document.addEventListener("DOMContentLoaded", e => {
    console.log(window.screenX);
    console.log(window.screenY);

    console.log(e)
});

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/** BOM: Metodos */

// alert('Hola!!!!!');
// confirm('Confirma');
// prompt('Ingresa tu nombre');
let ventana;

const $aVentana = document.getElementById("abrir-ventana");
const $cVentana = document.getElementById("cerrar-ventana");
const $iVentana = document.getElementById("imprimir-ventana");

console.log($aVentana);

$aVentana.addEventListener("click", e => {
    ventana = window.open("https://jonmircha.com");
});

$cVentana.addEventListener("click", e => {
    //window.close();
    ventana.close();
});

$iVentana.addEventListener("click", e => {
    window.print();
});


/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/** BOM: Objetos, URL, Historial y Navegador  */
console.log("*************Objeto URL *************");

console.log(location);
console.log(location.origin);
console.log(location.protocol);
console.log(location.host);
console.log(location.hostname);
console.log(location.port);
console.log(location.href);
console.log(location.hash);
console.log(location.search);
console.log(location.pathname);

console.log("*************Objeto History *************");
console.log(history);
// history.go(1 o -1)
// hostory.back(2)
// history.forward(1)


console.log("*************Objeto Navigator *************");
console.log(navigator);
console.log(navigator.userAgent);
console.log(navigator.platform);
console.log(navigator.languages);
console.log(navigator.connection);
console.log(navigator.mediaDevices);
console.log(navigator.onLine);
console.log(navigator.serviceWorker);
console.log(navigator.storage);
console.log(navigator.usb);
console.log(navigator.geolocation);
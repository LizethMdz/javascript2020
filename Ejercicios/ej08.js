const btn24 = document.getElementById('btn_24');
const btn25 = document.getElementById('btn_25');
const btn26 = document.getElementById('btn_26');

btn24.addEventListener('click', () => {
    let string = prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion'); 
    descAsc(string);
});
btn25.addEventListener('click', () => {
    let string = prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion');
    eliminarDuplicados(string);
});
btn26.addEventListener('click', () => {
    let string = prompt('Ingrese una serie de numeros delimitados por comas, para su evaluacion');
    obtenerPromedio(string);
});

function descAsc(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let tmpIndex = null;
    let asc = [];
    let desc = [];

    for(let k = 0; k < arrNumerico.length ; k++){
        asc.push(parseInt(arrNumerico[k]));
        desc.push(parseInt(arrNumerico[k]));
    }

    for (let i = 0; i < asc.length - 1; i++) {
        for (let j = 0; j < asc.length - i - 1; j++) {
            console.log(i, j);
            if(asc[j] > asc[j+1]){
                tmpIndex = asc[j];
                asc[j] = asc[j+1];
                asc[j+1] = tmpIndex; 
            }
            
        }
    }

    for (let i = 0; i < desc.length - 1; i++) {
        for (let j = 0; j < desc.length - i - 1; j++) {
            console.log(i, j);
            if(desc[j] < desc[j+1]){
                tmpIndex = desc[j];
                desc[j] = desc[j+1];
                desc[j+1] = tmpIndex; 
            }
            
        }
    }

    //Forma corta:
    // asc.sort((a,b) => a -b);
    // desc.sort((a,b) => b - a );
    let obj = {
        asc,
        desc
    }
    console.log(obj.asc + ' ---- ' +  obj.desc);
    return alert('El arreglo final es: ' + obj.asc + '---' + obj.desc);
    
}

function eliminarDuplicados(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let nuevoArray = arrNumerico.filter((valor,index)=>arrNumerico.indexOf(valor)===index);
    return alert('El arreglo final es: ' + nuevoArray);

}

function obtenerPromedio(arrNumerico){
    if(!arrNumerico || arrNumerico === undefined) alert('Introduce un arreglo de numeros');
    arrNumerico = arrNumerico.split(',');
    let suma = 0, promedio = 0;;
    let newArray = [];
    for(let k = 0; k < arrNumerico.length ; k++){
        newArray.push(parseInt(arrNumerico[k]));
    }
    for (let index = 0; index < newArray.length; index++) {
        suma += newArray[index];
        promedio = suma / newArray.length;
    }

    return alert('El promedio es: ' + promedio);
    //Forma corta:
    //const reducer = (accumulator, currentValue) => accumulator + currentValue;

    // 1 + 2 + 3 + 4
    //console.log(array1.reduce(reducer));
    // expected output: 10
    //const promedio=(array.reduce((acumulador, valor)=> acumulador+valor))/array.length;

}
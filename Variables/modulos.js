//import { PI } from "./anonimas.js"; 
console.log('ES 6');

/**
 * Import
 */

/**
  * import { PI } from "./anonimas.js"; 
  * Para que funcione la variable PI debe ser exportada con export
 */

/**
  * si una funcion se necesita correr por default
  * se declara de la siguiente manera
  * 
  * export default function saludar(){}
  * **************************
  * 
  * Para una variable:
  * let nombre = "liz";
  * export default nombre;
  * 
  * No se puede exportar de la siguiente manera:
  * export default let nombre = 'liz';
  */

  /**Pueden tener aliases */
    /**
   * import { arimetica as calculo } from "./arimetica.js"; 
   */
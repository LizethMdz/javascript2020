const btn09 = document.getElementById('btn_09');
const btn10 = document.getElementById('btn_10');
const btn11 = document.getElementById('btn_11');

btn09.addEventListener('click', () => {
    aleatorio();
});
btn10.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un numero'));
    numeroCapicua(valor);
});
btn11.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un valor'));
    factorial(valor);
});


function aleatorio(){
    return alert(Math.round(Math.random()* 100 + 500));
}

function numeroCapicua(valor){
    if(!valor || typeof valor !== 'number' ){
        alert('No ingreso numero');
    }else{
        let cadenaTmp = valor.split('').reverse().join('');
        if(cadenaTmp === valor){
            return alert('Es capicuo');
        }{
            return alert('No es capicuo');
        }
    }
    
}

function factorial(valor){
    if(!valor || typeof valor !== 'number' ){
        alert('No ingreso numero');
    }else if(Math.sign(valor) === -1){
        alert('Ingreso un numero negativo');
    }else{
        let res = 1;
        if(valor === 0){
            return 1;
        }
        for (let index = 1; index <= valor; index++) {
            res *= index; 
        }
        return alert('El resultado es: ' + res);
    }
}
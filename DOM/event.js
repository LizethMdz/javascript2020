/** Manejador de eventos */
const holaMundo = () => {
    alert("Hola mundo");
    // console.log(event);
}

// Evento semantico
const $eSemantico = document.getElementById("e-semantico");
$eSemantico.onclick = holaMundo;
// Al asignar otra funcion, no puede poseer mas eventos, sino que
// primero toma la primer funcion de referencia y despues continua
$eSemantico.onclick = function (e) {
    console.log(e);
}

// Evento  multiple
const $eventMultiple = document.getElementById("e-multiple");

$eventMultiple.addEventListener("click", holaMundo);
$eventMultiple.addEventListener("click", (e) => {
    alert('Hola mundo dentro desde otro funcion, en un manejador de eventos');
    console.log(e);
    console.log(e.target);
    console.log(e.type);
});


/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/** Eventos con parametros y Remover eventos */

function saludar(nombre = "Desconocido") {
    alert(`Hola ${nombre}`);
}

// De esta forma no puede recibir mas de un parametro, unicamente el evento
$eventMultiple.addEventListener("click", saludar);

// Entonces se tiende a usar funciones de flecha que se encargaran
// de manejar una nueva ejecucion de una funcion o codigo

$eventMultiple.addEventListener("click", () => {
    saludar();
});

// Eliminar eventos de la forma corecta

const $eRemover = document.getElementById('e-multiple-remover');

const removerDobleClick = (e) => {
    alert(`Removiendo el evento de tipo ${e.type}`);
    $eRemover.removeEventListener("dblclick", removerDobleClick);
    $eRemover.disabled = true;
}

$eRemover.addEventListener("dblclick", removerDobleClick);


/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/** DOM: Flujos de eventos (Burbuja y eventos)*/

// Tienen una propagacion a lo largo del DOM, sin embargo
// los evnetos suceden del mas interno al mas externo

// NOTE se comento por la seccion 76
/*const $divEventos = document.querySelectorAll(".e-flujo div");
console.log($divEventos);
*/
const flujoEventos = function (e) {
    console.log(this);
    console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
    // Integracion con laseccion 75
    e.stopPropagation();
}

// NOTE se comento por la seccion 76
/*$divEventos.forEach(div => {
    // Integracion con laseccion 75
    // Indica que es de burbuja
    // Fase de burbuja
    div.addEventListener("click", flujoEventos);
    div.addEventListener("click", flujoEventos, false);
    // Tambien el evento puede ir del mas externo al mas interno
    // Fase de captura
    div.addEventListener("click", flujoEventos, true);
    // Otra forma de especificar los eventos en cadena
    div.addEventListener("click", flujoEventos, {
        capture: true, // or false
        once: true // Ejecutara el evento solo una vez
    });

});*/

/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/** DOM: stopPropagation && preventDefault */

// Detener la propagacion
// Ejemplo
// const flujoEventos = function (e) {
//     console.log(this);
//     console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
//     e.stopPropagation();
// }

// NOTE se comento por la seccion 76
/*const $linkEventos = document.querySelector(".e-flujo a");

$linkEventos.addEventListener("click", (e) => {
    alert("Hola!!!!!!!!!!!!!!!!!!");
    // Detine la propagacion de los comportamientos que tenga en el DOM
    e.preventDefault();
    e.stopPropagation();
});*/

/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

/** DOM: Delgacion de eventos */

// Es mejor declarar un evneto al elemento padre,
// para no generar muchos eventos (click) y sea mas eficiente

// NOTE se comento la linea del foreach
document.addEventListener("click", (e) => {
    console.log("Click en ", e.target);
    if (e.target.matches(".e-flujo div")) {
        flujoEventos(e);
    }

    // Se establece una codicional para saber el selector
    if (e.target.matches(".e-flujo a")) {
        alert("Hola!!!!!!!!!!!!!!!!!!");
        // Detine la propagacion de los comportamientos que tenga en el DOM
        e.preventDefault();
    }
});

/**
 *
 *
 *
 *
 *
 *
 *





 */





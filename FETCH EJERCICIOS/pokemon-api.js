const d = document,
    $links = d.querySelector(".links"),
    $cards = d.getElementById("cards"),
    $template = d.getElementById("card-template").content,
    $fragment = d.createDocumentFragment();

let pokemonApi = "https://pokeapi.co/api/v2/pokemon/";
let pokemonApiIndividual = "https://pokeapi.co/api/v2/pokemon/";

const getPokemones = async (url) => {
    try {
        $cards.innerHTML = '<img class="loader" src="./assets/loader.svg" alt="Cargando..">';
        let response = await fetch(url);
        let json = await response.json(), $prevLink, $nextLink;

        console.log(json);

        if (!response.ok) throw { status: response.status, statusText: response.statusText }

        for (let i = 0; i < json.results.length; i++) {

            try {
                let responseIndividual = await fetch(`${json.results[i].url}`),
                    pokemon = await responseIndividual.json();
                //console.log(pokemon);
                if (!responseIndividual.ok) throw { status: responseIndividual.status, statusText: responseIndividual.statusText }
                $template.querySelector("img").src = pokemon.sprites.front_default;
                $template.querySelector("img").alt = pokemon.name;
                $template.querySelector("figcaption").innerHTML = `${pokemon.name}`;
                let $clone = d.importNode($template, true);
                $fragment.appendChild($clone);
            } catch (err) {
                //console.log(err);
                let message = err.statusText || "Ocurrió un error con el API Pokemones";
                $cards.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
            }

        }
        $cards.querySelector(".loader").classList.add("inactive");
        $cards.appendChild($fragment);
        $prevLink = json.previous ? `<a href="${json.previous}">⏮️</a>` : "";
        $nextLink = json.next ? `<a href="${json.next}">⏭️</a>` : "";
        $links.innerHTML = $prevLink + " " + $nextLink;

    } catch (err) {
        console.log(err);
        let message = err.statusText || "Ocurrió un error con el API Pokemones";
        $cards.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
    }
}

d.addEventListener("DOMContentLoaded", (e) => {
    getPokemones(pokemonApi);
});

d.addEventListener("click", e => {
    if (e.target.matches(".links a")) {
        e.preventDefault();
        getPokemones(e.target.getAttribute("href"));
    }
})
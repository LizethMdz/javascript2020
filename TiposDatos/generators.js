/**Funcion de tipo generica */

//Vuelve iterable a una funcion
function* iterable() {
    yield "hola";
    console.log("hola consola");
    yield "hola 2";
    console.log("Segunda linea");
    yield "hola 3";
    yield "hola 4";
}

let iterator = iterable();
console.log(iterator.next());

for (const y of iterator) {
    console.log(y);
}

const arr = [...iterable()];

console.log(arr);

function cuadrado(valor) {
    setTimeout(() => {
        return console.log({ valor, resultado: valor * valor });
    }, Math.random() * 1000);
}

function* generator() {
    console.log("Inicia Generator");
    yield cuadrado(0);
    yield cuadrado(1);
    yield cuadrado(2);
    yield cuadrado(3);
    yield cuadrado(4);
    console.log("Termina Generator");
}

let gen = generator();

for (const y of gen) {
    console.log(y);
}
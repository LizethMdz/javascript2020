const NOMBRE = Symbol("nombre");
const SALUDAR = Symbol("saludar");

//Oculta ciertas propiedades de un objeto de tipo privadas y unicas
const persona = {
    [NOMBRE]: "Liz",
    edad: 23
}

console.log(persona);

persona.NOMBRE = "LIZETH";

console.log(persona);

persona[SALUDAR] = () => {
    console.log('Hola');
};

console.log(persona);

persona[SALUDAR]();

for (const key in persona) {
    if (persona.hasOwnProperty(key)) {
        const element = persona[key];
        console.log(element);

    }
}

console.log(Object.getOwnPropertySymbols(persona));
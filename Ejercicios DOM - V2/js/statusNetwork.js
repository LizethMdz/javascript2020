const d = document,
    w = window,
    n = navigator;

export function statusNetwork() {


    const isOnline = () => {
        const $staus_n = d.getElementById("net-status");
        const $alerta = d.createElement("div");
        if (n.onLine) {
            $staus_n.innerHTML = "<p>Conectado</p>"
            $alerta.innerHTML = `<p>Se ha restablecido la conexión</p>`;
            $alerta.classList.add("network-notification");
            $alerta.classList.add("online");
            $alerta.classList.remove("offline");
        } else {
            $staus_n.innerHTML = "<p>Desconectado</p>"
            $alerta.innerHTML = `<p>Se ha perdidó la conexión</p>`;
            $alerta.classList.add("network-notification");
            $alerta.classList.add("offline");
            $alerta.classList.remove("online");
        }

        d.body.insertAdjacentElement("afterBegin", $alerta);
        setTimeout(() => d.body.removeChild($alerta), 3000);

    }

    w.addEventListener("online", (e) => {
        isOnline();
    })

    w.addEventListener("offline", (e) => {
        isOnline();
    })


}
const d = document,
    $cards = d.getElementById("cards"),
    $template = d.getElementById("card-template").content,
    $fragment = d.createDocumentFragment();


d.addEventListener("keypress", async (e) => {
    if (e.target.matches("#buscar")) {
        if (e.key === "Enter") {
            console.log("Se presiono");
            let valorBuscado = e.target.value.toLowerCase();
            try {
                $cards.innerHTML = '<img class="loader" src="./assets/loader.svg" alt="Cargando..">';
                let response = await fetch(`http://api.tvmaze.com/search/shows?q=${valorBuscado}`);
                let json = await response.json();

                if (!response.ok) throw { status: response.status, statusText: response.statusText }

                if (json.length === 0) {
                    $cards.innerHTML = `<p>No existen resultados de shows para el criterio <b>${valorBuscado}</b></p>`;
                }
                console.log(json);
                json.forEach(el => {
                    $template.querySelector("img").src = el.show["image"] ? el.show["image"]["medium"] : "http://static.tvmaze.com/images/no-img/no-img-portrait-text.png";
                    $template.querySelector("img").alt = el.show["name"];
                    $template.querySelector("figcaption").textContent = `${el.show["name"]}`;
                    $template.querySelector("h3").innerHTML = `${el.show["language"]}-${el.show["premiered"]}`;
                    $template.querySelector("p").innerHTML = el.show["summary"] ? `${el.show["summary"]}` : "Sin descripción";
                    $template.querySelector("a").href = el.show["url"] ? `${el.show["url"]}` : "#";
                    $template.querySelector("a").target = el.show["url"] ? `_blank` : "_self";
                    $template.querySelector("a").textContent = el.show["url"] ? "ver más..." : "";
                    let $clone = d.importNode($template, true);
                    $fragment.appendChild($clone);

                })

                $cards.querySelector(".loader") ? $cards.querySelector(".loader").classList.add("inactive") : "";
                $cards.appendChild($fragment);

            } catch (err) {
                console.log(err);
                let message = err.statusText || "Ocurrió un error con el API TVMAZE";
                $cards.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
            }
        }
    }
})
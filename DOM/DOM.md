### WEB API'S

1. DOM: DOCUMENT OBJECT MODEL
1. BOM: BROWSER OBJECT MODEL
1. CSSOM: CSS OBJECT MODEL

#### WEB APIS

- Eventos
- Forms
- AJAX - Fetch
- Web Storage
- Geolocalizacion
- Drag & Drop
- Indexed DB
- Canvas
- MachMedia
- etc.

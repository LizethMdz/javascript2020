const btn15 = document.getElementById('btn_15');
const btn16 = document.getElementById('btn_16');
const btn17 = document.getElementById('btn_17');

btn15.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un numero'));
    let valor2 = parseInt(prompt('Ingrese un la base 2 o 10 para convertir de Decimal a binario y veceversa'));
    binarioDecimal(valor, valor2);
});
btn16.addEventListener('click', () => {
    let valor = parseInt(prompt('Ingrese un numero'));
    let valor2 = parseInt(prompt('Ingrese el descuento a aplicar'));
    calcularDescuento(valor, valor2);
});
btn17.addEventListener('click', () => {
    let anio = parseInt(prompt('Ingrese un año YYYY:'));
    let mes = parseInt(prompt('Ingrese un mes MM:'));
    let dia = parseInt(prompt('Ingrese un dia DD:'));

    calcularAnios(anio, mes, dia);
});


function binarioDecimal(valor, valor2){

    if(!valor || typeof valor === "string" || typeof valor2 === 'string'){
        alert('Introduce un numero valido');
    }else if(valor < 1 || Math.sign(valor) === -1){
        alert('Introduce un numero mayor a uno por convencion!!');
    }else if(valor2  === 2){
        //Binario a Decimal
        //Forma lenta:
        let exp = 1;
        let residuo;
        let decimal = 0;
        if(valor == 0){
            decimal = 0;
        }else{
            while(valor > 0){
                residuo = valor % 10;
                valor = valor / 10;
                decimal += (residuo * exp);
                exp = 2 * exp;
            }
            let tmp = decimal - 1;
            return alert(tmp + ' base 10');  
        }
        
        //Forma rapida
        //return alert("El valor decimal es: " + valor.toString(10));
    }else if(valor2  === 10){
        //Decimal a Binario
        //Forma lenta:
        let x = 1;
        let binario  = 0;
        while(valor > 0){
            if(valor%2 === 1){
                binario = binario + x;
            }
            valor = valor/2;
            x = x * 10;
        }
        return alert(binario + ' base 2');  
        //Forma rapida
        //return alert("El valor binario es: " + valor.toString(2));
    }else{
        return alert('No ingreso una opcion valida');
    }
}


function calcularDescuento(valor, descuento){
    if(!valor || typeof valor === "string" || typeof valor2 === 'string'){
        alert('Introduce un numero valido');
    }else if(valor < 1 || Math.sign(valor) === -1 || valor2 < 1 || Math.sign(valor2) === -1){
        alert('Introduce un numero mayor a uno por convencion!!');
    }else{
        let des = (valor * descuento) / 100;
        let pagoFinal = valor - des;
        return alert('El pago final es: $' + pagoFinal);
    }
}


function calcularAnios(year, month, day){
    if(!year || !month || !day){
        alert('Debes introducir todos valores!!');
    }else if(isNaN(year) || isNaN(month) || isNaN(day) ){
        alert('Debes introducir valoresa validos!!');
    }else if(typeof year !== "number" || typeof month !== "number" || typeof day !== "number" ){
        alert('Debes introducir valoresa validos!!');
    }else if(Math.sign(year) === -1 || Math.sign(month) === -1 || Math.sign(day) === -1){
        alert('Debes introducir valoresa validos!!');
    }else{
        if(month > 12){
            alert('El mes no puede ser mayor a 12');
        }
        if(month > 31){
            alert('El dia no puede ser mayor a 31');
        }
        let fecha = new Date(year, month, day);
        let hoy = Date.now();
        let transcurrido = hoy - fecha;
        let miliPerYear = 31556900000;
        let years = Math.floor(transcurrido / miliPerYear);
        return alert('Los años ingresados: '+ years);
    }


}

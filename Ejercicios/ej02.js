const btn05 = document.getElementById('btn_05');
const btn06 = document.getElementById('btn_06');
const btn07 = document.getElementById('btn_07');
const btn08 = document.getElementById('btn_08');

btn05.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena');
    revirtirCaracteres(valor);
});
btn06.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena');
    let clave = prompt('Ingrese la palabra a encontrar');
    contarCaracteres(valor, clave);
});
btn07.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena para ver si es palindromo:');
    esPalindromo(valor);
});
btn08.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena');
    let clave = prompt('Ingrese la palabra a eliminar!');
    eliminarCaracter(valor, clave);
});

function revirtirCaracteres(cadena){
    let validacion = validateOnlyString(cadena);
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else {
        // let reverso = '';
        // for (let index = cadena.length-1; index >= 0; index--) {
        //     reverso += cadena[index];   
        // }
        // console.log(reverso);
        // return alert(reverso);

        //Solucion video
        return alert(cadena.split('').reverse().join(''));
        
    }  
}

function contarCaracteres(cadena, clave){
    let validacion = validateOnlyString(cadena);
    let validacion2 = validateOnlyString(clave); 
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else if(typeof clave != 'string' || validacion2){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else {
        let encontrado = cadena.includes(clave);
        let cadenaTransformada = cadena.split(' ');
        let contador = 0;
        if(encontrado){
            for (let index = 0; index < cadenaTransformada.length; index++) {
                let cadenaTmp = cadenaTransformada[index];
                if(cadenaTmp === clave){
                    contador++;
                }
            }
            return alert(`La palabra ${clave} se encontro ${contador} veces`);
        }else{
            return alert(`La palabra ${clave} no se encontro`);
        }

        //Solución del video
        // let i = 0;
        // let cont = 0;
        // while (i !== -1) {
        //     i = cadena.indexOf(clave, i);
        //     if(i !== -1){
        //         i++;
        //         cont ++;
        //     }
        // }
        // return cont;
    }
}

function esPalindromo(cadena){
    let validacion = validateOnlyString(cadena);
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else {
        let cadenaTmp = cadena.split('').reverse().join('').split(' ').join('');
        console.log(cadena.split(' ').join('') + ' | ' + cadenaTmp);
        if(cadenaTmp === cadena.split(' ').join('')){
            return alert('Es palindromo');
        }{
            return alert('No es palindromo');
        }
        
    }  
}

function eliminarCaracter(cadena, clave){
    let validacion = validateOnlyString(cadena);
    let validacion2 = validateOnlyString(clave); 
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else if(typeof clave != 'string' || validacion2){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else {
        //let cadenaTransformada = cadena.split(',');
        let regex = new RegExp(clave, "gi");
        let cadenaProcesada = [];
        //for (let index = 0; index < cadenaTransformada.length; index++) {
            //let element = cadenaTransformada[index].replace(regex, '');
            let element = cadena.replace(regex, '');
            console.log(element);
            //cadenaProcesada.push(element);
        //}
        //return alert(cadenaProcesada.join());
        return alert(element);
    }
}

function validateOnlyString(cadena){
    let validado = parseInt(cadena);
    if (isNaN(validado) === true){
        return false;
    }if( typeof validado === 'number') {
        return true;
    }else{
        return false;
    }
}
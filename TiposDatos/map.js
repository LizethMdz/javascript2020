/**Es mas parecido a los Objetos */

let mapa = new Map();

mapa.set("nombre", 'John');
mapa.set("apellido", 'Mircha');

console.log(mapa);

console.log(mapa.has("nombre"));

console.log(mapa.get("nombre"));

mapa.set("nombre", "Jonathan");

// Se puede establecer una llave de tipo int, string, objetos, booleanes y asi sucevivamente
// Ej. mapa.set(19, "diecinueve");

console.log(mapa);

mapa.delete("nombre");

for (let [key, value] of mapa) {
    console.log(key, value);
}


// Agregar una propiedad de tipo arreglo

const mapa2 = new Map([
    ["nombre", "flay"],
    ["edad", 5],
    ["animal", "perro"],
    [null, "nulo"]
]);

console.log(mapa2);

for (const iterator of mapa2) {
    console.log(iterator);
}

console.log('---NOTE--USO DE ... ---');
const obj = {
    foo: {
        a: 1,
        b: 2,
        c: 3
    }
};
console.log("original", obj.foo);
// Creates a NEW object and assigns it to `obj.foo`
obj.foo = { ...obj.foo, a: "updated" };
console.log("updated", obj.foo);
console.log('---NOTE-----');


const llaves = [...mapa2.keys()];
const valores = [...mapa2.values()];

console.log(llaves);
console.log(valores);


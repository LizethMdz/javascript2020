/**Prototipos */

/**
 * Clases,
 * Objetos:
 *  Atributos
 *  Metodos
 */

 /**Prototipo */

//Funcion constructora
function Animal (nombre, genero){
    this.nombre = nombre;
    this.genero = genero;

    /**De esta manera gasta mas recursos, ya que se crea en cada instancia */
    this.sonar = function () {
        console.log('Sonidos!!');
    }
}

/**La mejor manera de agregar metodos es la siguiente */
Animal.prototype.saludar = function () {
    console.log('Me llamo: ' + this.nombre);
}

let snoppy = new Animal('Cachis', 'hembra');
snoppy.saludar();
snoppy.sonar();
const d = document;

export function slider() {
    const $nextBtn = d.querySelector(".slider-btns .next"),
        $prevBtn = d.querySelector(".slider-btns .prev"),
        $slides = d.querySelectorAll(".slider-slide");

    let contador = 0;
    console.log($slides);
    d.addEventListener("click", (e) => {
        if (e.target === $prevBtn) {
            e.preventDefault();
            $slides[contador].classList.remove("active");
            contador--;
            if (contador < 0) {
                contador = $slides.length - 1;
            }

            $slides[contador].classList.add("active");
        }
        if (e.target === $nextBtn) {
            e.preventDefault();
            $slides[contador].classList.remove("active");
            contador++;
            if (contador >= $slides.length) {
                contador = 0;
            }

            $slides[contador].classList.add("active");
        }
    });

}
const d = document,
    $main = d.querySelector("main"),
    $drop_zone = d.querySelector(".drop-zone");
const $files = d.getElementById("files");

const uploader = (file) => {
    console.log(file);
    const xhr = new XMLHttpRequest(),
        formData = new FormData();

    formData.append("file", file);
    xhr.addEventListener("readystatechange", (e) => {
        if (xhr.readyState !== 4) return;
        console.log("Entro");
        if (xhr.status >= 200 && xhr.status < 300) {
            let json = JSON.parse(xhr.responseText);
            console.log(json);
            //console.log(xhr.responseText);
        } else {
            let message = xhr.statusText || "Ocurrió un error";
            console.log(`Error ${xhr.status}: ${message}`);
        }
    });
    xhr.open("POST", "uploader.php");
    xhr.setRequestHeader("enc-type", "multipart/form-data");
    xhr.send(formData);
}

const progressUpload = (file) => {
    const $progress = d.createElement("progress"),
        $span = d.createElement("span");

    $progress.max = 100;
    $progress.value = 0;

    $main.insertAdjacentElement("beforeend", $progress);
    $main.insertAdjacentElement("beforeend", $span);

    const fileReader = new FileReader();

    fileReader.readAsDataURL(file);

    fileReader.addEventListener("progress", e => {
        let progress = parseInt(e.loaded * 100 / e.total);
        $progress.value = progress;
        $span.innerHTML = `<b>${file.name} - ${progress}%</b>`;
    });

    fileReader.addEventListener("loadend", e => {
        uploader(file);
        setTimeout(() => {
            $main.removeChild($progress);
            $main.removeChild($span);
            if ($files.value !== "") {
                $files.value = "";
            }
        }, 3000);
    });
}

$drop_zone.addEventListener("dragover", (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.target.classList.add("is-active");
});
$drop_zone.addEventListener("dragleave", (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.target.classList.remove("is-active");
});
$drop_zone.addEventListener("drop", (e) => {
    e.preventDefault();
    e.stopPropagation();
    const files = Array.from(e.dataTransfer.files);
    files.forEach(el => {
        progressUpload(el);
    });
    e.target.classList.remove("is-active");
});


d.addEventListener("change", (e) => {
    if (e.target === $files) {
        console.log(e.target.files);
        // Le da la capacidad de que tenga metodos de los arreglos
        const files = Array.from(e.target.files);

        files.forEach(el => {
            progressUpload(el);
        });

    }
});
class Animal {
    
    constructor(nombre, genero){
        this.nombre = nombre;
        this.genero = genero;
    }

    saludar () {
        console.log('Me llamo: ' + this.nombre);
    }
}

let mimi = new Animal('Mimi', 'Hembra');
let mickey = new Animal('Mickey', 'Macho');

mimi.saludar();
mickey.saludar();

class Perro extends Animal {

    constructor(nombre, genero, tamanio){
        super(nombre, genero);
        this.tamanio = tamanio;
    }

    saludar() {
        console.log('wau wau!!');
    }

    gruñir(){
        console.log('rrrrrrr rrrrrr!');
    }

    //Metodo estatico
    static queEs(){
        console.log("Somo perros!!");
    }
}

let scooby = new Perro('Scooby', 'Macho', 'Mediano');

scooby.gruñir();
scooby.saludar();

Perro.queEs();
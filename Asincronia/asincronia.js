/**Temporizadores */
setTimeout(() => {
    console.log('Ejecutando este setTime');
}, 1000);

// Se ejecuta n cantidad de veces
// setInterval(() => {
//     console.log('Ejecutando este setTime');
// }, 1000);

let relog = setTimeout(() => {
    console.log(new Date().toLocaleTimeString());
});

clearTimeout(relog);

/**Asincronia y event Loop */

// Single thread y Multi thread
// Operaciones de CPU y operaciones de Input/ Output
// Operaciones concurrentes y paralelas
// Concurrente - Varias tareas se ejecutan progresivamente
// Paralelas - Varias tareas se ejecutan al mismo tiempo 
// Operaciones Bloqueantes - Ejecuta toda su tarea y hasta que termine
// devuelve su respuesta.
// Operaciones No bloqueantes - Se jecutan todas y devuelven su respuesta al
// hilo principal si han o no terminado su proceso.
// Sincrono - Respuesta es devuelta en el presente
// Asincrono - Respuesta que pude devolver el control al hilo principal

/**Sincrono bloqueante */

// (() => {
//     console.log('Codigo sincrono');
//     console.log('Inicio');

//     function dos(){
//         console.log('dos');
//     }

//     function uno(){
//         console.log('uno');
//         dos();
//         console.log('tres');
//     }

//     uno();
//     console.log('Fin');
// });

/**Asincrono bloqueante */

// console.log('Codigo sincrono');
// console.log('Inicio');

// function dos(){
//     setTimeout(function (){
//         console.log('dos');
//     }, 1000);

// }

// function uno(){
//     setTimeout(function (){
//         console.log('uno');
//     }, 1000);

//     dos();
//     console.log('tres');
// }

// uno();
// console.log('Fin');

// Herramienta Loupe http://latentflip.com/loupe/

/**
 * Resumen
 * Java script uso un modelo sincrono y
 * no bloqueante con un loop de eventos implementando
 * en un solo hilo (single thread para operaciones
 * de entrada y salida).
 */

/**Callbacks */
// function  cuadradoCallback(value, callback){
//     setTimeout(() => {
//         callback(value, value * value);
//     }, 0 | Math.random() * 1000);
// }

// cuadradoCallback(0, (value, result) => {
//     console.log('Inicia Callback');
//     console.log(`Callback ${value}, ${result}`);
//     cuadradoCallback(1, (value, result) => {
//         console.log(`Callback ${value}, ${result}`);
//         cuadradoCallback(3, (value, result) => {
//             console.log(`Callback ${value}, ${result}`);
//         });
//     });
// });

/**Promesas */

// function  cuadradoPromise(value){
//     if (typeof value !== "number") return Promise.reject(`Error
//     el valor ${value} no es numero`);
//     return new Promise((resolve, reject)=> {
//         setTimeout(() => {
//             resolve({
//                 value,
//                 result: value * value
//             });
//         }, 0 | Math.random() * 1000);
//     });

// }

// cuadradoPromise(0)
//     .then((obj) => {
//         console.log(obj);
//         return cuadradoPromise(1);
//     })
//     .then((obj) => {
//         console.log(obj);
//         return cuadradoPromise(2);
//     })
//     .then((obj) => {
//         console.log(obj);
//         return cuadradoPromise("3");
//     })
//     .catch(err =>{
//         console.error(err);
//     });

/**Await y Async */

// function cuadradoPromise(value) {
//     if (typeof value !== "number") return Promise.reject(`Error
//     el valor ${value} no es numero`);
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve({
//                 value,
//                 result: value * value
//             });
//         }, 0 | Math.random() * 1000);
//     });

// }

// async function funcionAsincrona() {
//     try {
//         console.log('Inicio Async Funcion');
//         let obj = await cuadradoPromise(5);
//         console.log(`Async Funcion ${obj.value}, ${obj.result}`);

//         obj = await cuadradoPromise(4);
//         console.log(`Async Funcion ${obj.value}, ${obj.result}`);
//     } catch (err) {
//         console.log(err);
//     }
// }

// funcionAsincrona();

// const funcionExpresada = async () => {
//     console.log('Inicio Async Funcion');
//     let obj = await cuadradoPromise(5);
//     console.log(`Async Funcion ${obj.value}, ${obj.result}`);

//     obj = await cuadradoPromise(4);
//     console.log(`Async Funcion ${obj.value}, ${obj.result}`);
//     console.log('Fin de la funcion');
// }

/**Simbolos */


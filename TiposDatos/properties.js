/**propiedades dinamicas */

const objUsuarios = {};

const usuarios = ["Jon", "Irma", "Miguel", "Karla"];

console.log(usuarios);

usuarios.forEach((usuario, index) => objUsuarios[`id_${index}`] = usuario);

console.log(objUsuarios);

/**Crear propiedades dinamicas directametne en el objeto */

const objUsuarios2 = {
    [`id_${Math.round(Math.random() * 100 + 5)}`]: "Valor aleatorio"
};

console.log(objUsuarios2);

/**Palabra reservada this */

// La arrow function no genera un scope, por lo que le antece el padre

// Las funciones normales si crean su propio contexto


// Funcion dentro de otra funcion  - Clausulas

this.nombre = "Contexto global";

function Persona(nombre) {
    this.nombre = nombre;

    return function () {
        // Crea un nuevo scope (contexto), por lo que no conoce la variable this.nombre
        // Imprime el valor de nombre del contexto global 
        console.log(this.nombre, 22);
    }

    // Con el uso de arrow functions, ya que no crea un nuevo contexto
    return () => {
        // Este seria la solucion
        console.log(this.nombre, 22);
    }
}

let lizeth = new Persona("lizeth");
lizeth();
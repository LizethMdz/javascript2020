const d = document,
    w = window;

export function narrador() {
    let synth = w.speechSynthesis;
    const $inputTxt = d.querySelector('#speech-text');
    const $voiceSelect = d.querySelector('#speech-select');
    const $speachBtn = d.querySelector("#speech-btn");

    let voices = [];

    const populateVoiceList = () => {
        voices = synth.getVoices();
        console.log(voices);

        for (let i = 0; i < voices.length; i++) {
            const $option = d.createElement('option');
            $option.textContent = voices[i].name + ' (' + voices[i].lang + ')';

            if (voices[i].default) {
                $option.textContent += ' -- DEFAULT';
            }

            $option.setAttribute('data-lang', voices[i].lang);
            $option.setAttribute('data-name', voices[i].name);
            $voiceSelect.appendChild($option);
        }
    }


    d.addEventListener("DOMContentLoaded", (e) => {

        speechSynthesis.addEventListener("voiceschanged", (e) => {
            populateVoiceList();
        });

    });



    d.addEventListener("click", (e) => {
        e.preventDefault();
        if (e.target === $speachBtn) {

            let utterThis = new SpeechSynthesisUtterance($inputTxt.value);
            let selectedOption = $voiceSelect.selectedOptions[0].getAttribute('data-name');
            for (let i = 0; i < voices.length; i++) {
                if (voices[i].name === selectedOption) {
                    utterThis.voice = voices[i];
                }
            }
            //   utterThis.pitch = pitch.value;
            //   utterThis.rate = rate.value;
            synth.speak(utterThis);

            $inputTxt.blur();
        }

    });
}
class Pelicula {
    constructor({id, titulo, director, anio, pais, genero, calificacion}) {
        this.id = id;
        this.titulo = titulo;
        this.director = director;
        this.anio = anio;
        this.pais = pais;
        this.genero = genero;
        this.calificacion = calificacion;

        this.valId(id);
        this.valDirector(director);
        this.valAnio(anio);
        this.valGeneros(genero);
        this.valPais(pais);
        this.validarCal(calificacion);
    }

    valId(id){
        let reg = /^([A-Za-z]{2}([0-9]{7}))$/;
        if(!reg.test(id) || typeof id !== 'string'){
            console.log('El id no tiene al formato correcto (AA0000000)');
        }
    }

    valAnio(anio){
        let patron = /^([0-9]{4})$/;
        if(!patron.test(anio) || typeof anio !== 'number'){
            console.log('El año no tiene al formato correcto (YYYY)');
        }
    }

    valPais(pais){
        if(! (pais instanceof Array ) || pais.length === 0){
            console.log("El formato ingresado no es valido. (['alemania', 'mexico']) ");
        }
        for(let element of pais){
            if(typeof element !== 'string'){
                console.log("El formato ingresado no es valido. (['alemania', 'mexico']) ");
            }
        }
    }

    valDirector(director){
        if(typeof director !== 'string' || director.length > 50){
            console.log("El formato ingresado no es valido. (Menor a 50 caracteres)");
        }
    }

    valGeneros(genero){
        if(! (genero instanceof Array ) || genero.length === 0){
            console.log("El formato ingresado no es valido. ([''..]) ");
        }
        for(let element of genero) {
            if(typeof element !== 'string'){
                console.log("El formato ingresado no es valido. (['alemania', 'mexico']) ");
            }
            if(!Pelicula.generosAceptados().includes(element)){
                console.log('El genero no esta incluido!!');
            }
        }
    }

    static generosAceptados() {
        return ['Action', 'Adult', 'Adventure', 'Animation', 'Biography', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Family', 'Fantasy', 'Film Noir', 'Game-Show', 'History', 'Horror', 'Musical', 'Music'
            , 'Mystery', 'News', 'Reality-TV', 'Romance', 'Sci-Fi', 'Short', 'Sport', 'Talk-Show', 'Thriller', 'War', 'Western'];
    }

    validarCal(calificaion) {
        if (typeof calificaion === 'number' && (calificaion > 1 && calificaion < 10)) {
            return true;
        }
    }

    fichaPelicula() {
        console.log( 'Nombre: ' + this.titulo + ' Director: ' + this.director + ' Año: ' + this.anio + ' Calificacion: ' + this.calificacion
        +   ' Generos: ' + this.genero + ' Paises: ' + this.pais);
    }
}



const misPelis = [
    {
        id: "tt0758758",
        titulo: "Into the Wild",
        director: "Sean Penn",
        anio: 2007,
        pais: ["USA"],
        genero: ["Adventure", "Biography", "Drama"],
        calificacion: 8.1,
    },
    {
        id: "tt0479143",
        titulo: "Rocky Balboa",
        director: "Sylbester Stallone",
        anio: 2006,
        pais: ["USA"],
        genero: ["Action", "Drama", "Sport"],
        calificacion: 7.1,
    },
    {
        id: "tt0468569",
        titulo: "The Dark Knight",
        director: "Christopher Nolan",
        anio: 200,
        pais: ["USA", "UK"],
        genero: ["Action", "Crime", "Drama"],
        calificacion: 9.0,
    },
];
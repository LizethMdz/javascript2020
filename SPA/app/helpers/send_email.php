<?php 

if (isset($_POST)) {
    error_reporting(0);
    $name = $_POST["name"];
    $email = $_POST["email"];
    $subject = $_POST["subject"];
    $comments = $_POST["comments"];
    $domain = $_SERVER["HTTP_HOST"];
    $to = "stupsidedown@gmail.com";
    $subject = "Contacto desde el formulario del sitio $domain: $subject";
    $mensaje = "
        <ul>
            <li>Nombre: $name</li>
            <li>Email: $email</li>
            <li>Asunto: $subject</li>
            <li>Comentarios: $subject</li>
        </ul>

    ";

    $headers = "MIME-Version: 1.0\r\n"."Content-Type: text/html; charset=utf8\r\n"."From: Envío Automatico <no-reply@$domain>";

    $send_mail = mail($to, $subject, $mensaje, $headers);

    if ($send_mail) {
        $res = [
            "err" => false,
            "message" => "Tus datos han sido enviados"
        ];
    }else{
        $res = [
            "err" => true,
            "message" => "Error al enviar tus datos, Intenta de nuevo"
        ];
    }

    header("Access-Control-Allow-Origin: http://127.0.0.1");
    header("Content-type: application/json");
    echo json_encode($res);
    exit;
}

?>
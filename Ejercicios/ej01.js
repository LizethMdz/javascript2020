const btn01 = document.getElementById('btn_01');
const btn02 = document.getElementById('btn_02');
const btn03 = document.getElementById('btn_03');
const btn04 = document.getElementById('btn_04');

btn01.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena');
    contarCaracteres(valor);
});
btn02.addEventListener('click', () => {
    let string = prompt('Ingrese una cadena');
    let num = parseInt(prompt('Ingresa el numero de caracteres a recortar'));
    recortarCaracteres(string, num);
});
btn03.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena para separar, por defecto esta el " " ');
    let separador = prompt('Ingrese un separador');
    separarCaracteres(valor, separador);
});
btn04.addEventListener('click', () => {
    let valor = prompt('Ingrese una cadena');
    let repetidor = parseInt(prompt('Ingrese el numero de veces!'));
    repetirCaracteres(valor, repetidor);
});

function contarCaracteres(cadena){
    let validacion = validateOnlyString(cadena);
    if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Es necesario una cadena!!');
    }else {
        console.log('El numero de caracteres es: ' + cadena.length);
        return alert(cadena.length);
    }
}

function recortarCaracteres(cadena, number){
    let validacion = validateOnlyString(cadena);
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    } else if(!number || typeof number != 'number' || isNaN(number)){
        console.warn('Por favor revisa los caracteres introducidos');
        alert('No haz introducido un numero!!');
    }else {
        number = Math.abs(number);
        number = Math.round(number);
        console.log(cadena.substring(0, number));
        return alert("Se recortó a " + number + " caracteres: "  + cadena.substring(0, number));
    }  
}
    


function separarCaracteres(cadena, separador){
    let nuevaCadena = null;
    let validacion = validateOnlyString(cadena);
    console.log(validacion);
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else{
        nuevaCadena = cadena.split(separador);
        console.log(nuevaCadena);   
        return alert(nuevaCadena); 
    }
}

function repetirCaracteres(cadena, repetidor){
    let validacion = validateOnlyString(cadena);
    if(!cadena){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida!!');
    } else if(typeof cadena != 'string' || validacion){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('Debe ser una cadena valida no se permiten numeros en ella!!');
    }else if(!repetidor || typeof repetidor == 'string' || isNaN(repetidor)){
        console.warn('Por favor revisa los carecteres introducidos');
        alert('No haz introducido un numero!!');
    }else{
        repetidor = Math.abs(repetidor);
        repetidor = Math.round(repetidor);
        cadena = cadena + " | ";
        console.log(cadena.repeat(repetidor));   
        return alert("Se repitió " + repetidor + " veces: "  + cadena.repeat(repetidor)); 
    }
}

function validateOnlyString(cadena){
    let validado = parseInt(cadena);
    console.log(validado);
    
    if (isNaN(validado) === true){
        return false;
    }if( typeof validado === 'number') {
        return true;
    }else{
        return false;
    }
}
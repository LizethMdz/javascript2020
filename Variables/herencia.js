function Animal (nombre, genero){
    this.nombre = nombre;
    this.genero = genero;
}

/**La mejor manera de agregar metodos es la siguiente */
Animal.prototype.saludar = function () {
    console.log('Me llamo: ' + this.nombre);
}

let snoppy = new Animal('Cachis', 'hembra');


//Herencia prototipica

function Perro(nombre, genero, tamanio){
    this.super = Animal;
    this.super(nombre, genero);
    this.tamanio = tamanio;
}

//Perro esta heredando
Perro.prototype = new Animal();

Perro.prototype.constructor = Perro;

//Sobreescritura de metodos

Perro.prototype.saludar = function () {
    console.log('wau wau wai');
}

let flay = new Perro('Flay', 'hembra', 'Mediano');

console.log(snoppy);
console.log(flay);

snoppy.saludar();
flay.saludar();
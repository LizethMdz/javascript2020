console.log(this);

this.lugar = "contexto global";

function saludar(saludo, aQuien) {
    console.log(`${saludo} ${aQuien} desde el ${this.lugar}`);
}

this.nombre = "Window";

saludar();

const obj = {
    lugar: "Contexto objeto"
}

// Recibe un objeto como el contexto
saludar.call(obj, "Hola", "Lizz");
saludar.call(null, "Hola", "Lizz");
saludar.call(this, "Hola", "Lizz");

// Recibe el objeto mas un arreglo de parametros
saludar.apply(obj, ['Adios', 'Liz']);
saludar.apply(null, ['Adios', 'Liz']);
saludar.apply(this, ['Adios', 'Liz']);

const persona = {
    nombre: 'Liz',
    saludar: function () {
        console.log(`Hola!!!!!! ${this.nombre}`);
    }
}

persona.saludar();

const persona2 = {
    // No tiene el contexto de nadie
    // Asi que se tiene que enlazar el contexto de alguien, por ejemplo: Persona
    saludar: persona.saludar.bind(persona),
    // Asi que se tiene que enlazar el contexto de alguien, por ejemplo: this (Window)
    saludar2: persona.saludar.bind(this)
}

persona2.saludar();

persona2.saludar2();
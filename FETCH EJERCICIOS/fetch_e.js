const d = document;

function contactFormValidations() {
    const $form = d.querySelector(".contact-form");
    const $inputs = d.querySelectorAll(".contact-form [required]");

    $inputs.forEach((input) => {
        const $span = d.createElement("span");
        $span.id = input.name;
        $span.textContent = input.title;
        $span.classList.add("contact-form-error", "none");
        input.insertAdjacentElement("afterend", $span);
    });

    d.addEventListener("keyup", (e) => {
        if (e.target.matches(".contact-form [required]")) {
            let $input = e.target, pattern = $input.pattern || $input.dataset.pattern;
            // console.log($input, pattern);

            // Validaciones de patrones
            if (pattern) {
                let regexp = new RegExp(pattern);
                return !regexp.exec($input.value) ? d.getElementById($input.name).classList.add("is-active")
                    : d.getElementById($input.name).classList.remove("is-active")
            }

            if (!pattern) {
                return $input.value === "" ? d.getElementById($input.name).classList.add("is-active")
                    : d.getElementById($input.name).classList.remove("is-active")
            }
        }
    });
    d.addEventListener("submit", (e) => {
        e.preventDefault();
        //alert("Enviando formulario");
        const $loader = d.querySelector(".contact-form-loader"),
            $response = d.querySelector(".contact-form-response");

        $loader.classList.remove("none");

        fetch("send_email.php",
            {
                method: "POST",
                body: new FormData(e.target),
                mode: "cors"
            }
        )
            .then((res) => (res.ok ? res.json() : Promise.reject(res)))
            .then((json) => {
                $loader.classList.add("none");
                $response.classList.remove("none");
                $form.reset();
                $response.innerHTML = `<p>${json.message}</p>`;
                //console.log(json);
            })
            .catch((err) => {
                //console.log(err);
                let message = err.statusText || "Ocurrió un error";
                $response.innerHTML = `Error ${err.status} | ${message}`;
            })
            .finally(() => setTimeout(() => {
                $response.classList.add("none");
                $response.innerHTML = "";
            }, 3000));
    });
}

d.addEventListener("DOMContentLoaded", contactFormValidations());

import api from "../helpers/wp_api.js";
import { ajax } from "../helpers/ajax.js";
import { PostCard } from "./PostCard.js";
import { Post } from "./Post.js";
import { SearchCard } from "./SearchCard.js";
import { ContactForm } from "./ContactForm.js";

export const Router = async () => {
    const d = document,
        w = window,
        $main = d.getElementById("main");
    let { hash } = location;

    $main.innerHTML = null;

    if (!hash || hash === "#/") {
        await ajax({
            url: api.POSTS,
            cbSuccess: (posts) => {
                console.log(posts);
                let html = "";
                posts.forEach(element => html += PostCard(element));
                $main.innerHTML = html;
            }
        });
        //$main.innerHTML = `<h2>Sección del Home</h2>`;
    } else if (hash.includes("#/search")) {
        $main.innerHTML = `<h2>Sección del Buscador</h2>`;
        let query = localStorage.getItem("wpSearch");

        if (!query) {
            d.querySelector(".loader").style.display = "none";
            return false;
        }

        await ajax({
            url: `${api.SEARCH}${query}`,
            cbSuccess: (search) => {
                console.log(search);
                let html = "";
                if (search.length === 0) {
                    html = `
                        <p class="error">
                            No existen resultdos de búsqueda para el 
                            término <mark>${query}</mark>
                        </p>
                    `;
                    $main.innerHTML = html;
                } else {
                    search.forEach(element => html += SearchCard(element));
                    $main.innerHTML = html;
                }
            }
        });
    } else if (hash.includes("#/contacto")) {
        $main.appendChild(ContactForm());
    } else {
        await ajax({
            url: `${api.POST}/${localStorage.getItem("wpPostId")}`,
            cbSuccess: (post) => {
                console.log(post);
                $main.innerHTML = Post(post);
            }
        });
    }

    d.querySelector(".loader").style.display = "none";

    console.log(hash);
}
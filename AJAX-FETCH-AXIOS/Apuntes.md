AJAX significa Asynchronous JavaScript And XML. En pocas palabras, es el uso del objeto XMLHttpRequest para comunicarse con los servidores.

Puede enviar y recibir información en varios formatos, incluidos JSON, XML, HTML y archivos de texto.

El atractivo de AJAX es su naturaleza "asíncrona", lo que significa que puede comunicarse con el servidor, intercambiar datos y actualizar la página sin tener que recargar el navegador.

Modelo AJAX

1. Métodos Nativos

- ActiveXObject (IE8 e inferiores, esta depreciado)
- XMLHttpRequest
- API Fetch

1. Librerías Externas

- jQuery.ajax()
- Axios
- etc.

AJAX no es una tecnología en sí mismo. En realidad, se trata de varias tecnologías independientes que se unen:

- HTML y CSS, para crear una presentación basada en estándares.
- DOM, para la interacción y manipulación dinámica de la presentación.
- HTML, XML y JSON, para el intercambio y la manipulación de información.
- XMLHttpRequest o Fetch, para el intercambio asíncrono de información.
- JavaScript, para unir todas las demás tecnologías.
- Es importante también considerar los Códigos de estado de respuesta HTTP y los estados de la petición AJAX:

Estado Valor

- READY_STATE_UNINITIALIZED 0
- READY_STATE_LOADING 1
- READY_STATE_LOADED 2
- READY_STATE_INTERACTIVE 3
- READY_STATE_COMPLETE 4
  Ver Video

### Objeto XMLHttpRequest

const xhr = new XMLHttpRequest(),
$xhr = document.getElementById("xhr"),
  $fragment = document.createDocumentFragment();

xhr.addEventListener("readystatechange", (e) => {
if (xhr.readyState !== 4) return;

console.log(xhr);

if (xhr.status >= 200 && xhr.status < 300) {
console.log("éxito");
console.log(xhr.responseText);
//\$xhr.innerHTML = xhr.responseText;
let json = JSON.parse(xhr.responseText);
console.log(json);

    json.forEach((el) => {
      const $li = document.createElement("li");
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $xhr.appendChild($fragment);

} else {
console.log("error");
let message = xhr.statusText || "Ocurrió un error";
\$xhr.innerHTML = `Error ${xhr.status}: ${message}`;
}

console.log("Este mensaje cargará de cualquier forma");
});

xhr.open("GET", "https://jsonplaceholder.typicode.com/users");
//xhr.open("GET", "assets/users.json");

xhr.send();
Ver Video

## API Fetch

const $fetch = document.getElementById("fetch"),
  $fragment = document.createDocumentFragment();

//fetch("assets/users.json")
fetch("https://jsonplaceholder.typicode.com/users")
/_ .then((res) => {
console.log(res);
return res.ok ? res.json() : Promise.reject(res);
}) _/
.then((res) => (res.ok ? res.json() : Promise.reject(res)))
.then((json) => {
console.log(json);
//$fetch.innerHTML = json;
    json.forEach((el) => {
      const $li = document.createElement("li");
\$li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
$fragment.appendChild($li);
});

    $fetch.appendChild($fragment);

})
.catch((err) => {
console.log(err);
let message = err.statusText || "Ocurrió un error";
\$fetch.innerHTML = `Error ${err.status}: ${message}`;
})
.finally(() => {
console.log(
"Esto se ejecutará independientemente del resultado de la Promesa Fetch"
);
});
Ver Video

### API Fetch + (Async - Await)

const $fetchAsync = document.getElementById("fetch-async"),
  $fragment = document.createDocumentFragment();

async function getData() {
try {
let res = await fetch("https://jsonplaceholder.typicode.com/users"),
json = await res.json();

    console.log(res, json);

    //if (!res.ok) throw new Error("Ocurrio un Error al solicitar los Datos");
    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    json.forEach((el) => {
      const $li = document.createElement("li");
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $fetchAsync.appendChild($fragment);

} catch (err) {
console.log(err);
let message = err.statusText || "Ocurrió un error";
\$fetchAsync.innerHTML = `Error ${err.status}: ${message}`;
} finally {
console.log("Esto se ejecutará independientemente del try... catch");
}
}

getData();
Ver Video

#### Librería Axios

const $axios = document.getElementById("axios"),
  $fragment = document.createDocumentFragment();

axios
//.get("assets/users.json")
.get("https://jsonplaceholder.typicode.com/users")
.then((res) => {
console.log(res);
let json = res.data;

    json.forEach((el) => {
      const $li = document.createElement("li");
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $axios.appendChild($fragment);

})
.catch((err) => {
console.log(err.response);
let message = err.response.statusText || "Ocurrió un error";
\$axios.innerHTML = `Error ${err.response.status}: ${message}`;
})
.finally(() => {
console.log("Esto se ejecutará independientemente del resultado Axios");
});

### Librería Axios + (Async - Await)

const $axiosAsync = document.getElementById("axios-async"),
  $fragment = document.createDocumentFragment();

async function getData() {
try {
let res = await axios.get("https://jsonplaceholder.typicode.com/users"),
json = await res.data;

    console.log(res, json);

    json.forEach((el) => {
      const $li = document.createElement("li");
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $axiosAsync.appendChild($fragment);

} catch (err) {
console.log(err.response);
let message = err.response.statusText || "Ocurrió un error";
\$axiosAsync.innerHTML = `Error ${err.response.status}: ${message}`;
} finally {
console.log("Esto se ejecutará independientemente del try... catch");
}
}

getData();

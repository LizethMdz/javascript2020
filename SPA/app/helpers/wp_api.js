const NAME = "css-tricks";
const DOMAIN = `https://${NAME}.com`,
    SITE = `${DOMAIN}/wp-json`,
    API_WP = `${SITE}/wp/v2`,
    PER_PAGE = 6,
    POST = `${API_WP}/posts`,
    POSTS = `${API_WP}/posts?_embed&per_page=${PER_PAGE}`,
    SEARCH = `${API_WP}/search?_embed&per_page=${PER_PAGE}&search=`;

let page = 1;

export default {
    NAME,
    DOMAIN,
    SITE,
    API_WP,
    POSTS,
    POST,
    SEARCH,
    PER_PAGE,
    page
}
const btn18 = document.getElementById('btn_18');
const btn19 = document.getElementById('btn_19');
const btn20 = document.getElementById('btn_20');

btn18.addEventListener('click', () => {
    let string = prompt('Ingrese un texto');
    validarMayusculas(string);

});
btn19.addEventListener('click', () => {
    let string = prompt('Ingrese un nombre');
    validarNombre(string);
});
btn20.addEventListener('click', () => {
    let string =prompt('Ingrese un email:');
    validarEmail(string);
});

function validarMayusculas(string){
    let tmpArr = string.split('');
    let v = 0, c = 0;

    for (let index = 0; index < tmpArr.length; index++) {
        if(/(a|e|i|o|u)/gi.test(string[index])){
            v++;
        }else if(/([b-d]|[f-h]|[j-n]|[p-t]|[v-z])/gi.test(tmpArr[index])){
            c++;
        }
    }

    return alert('Numero de  consonantes: ' + c + ' No. de Vocales: ' + v);
}

function validarNombre(string){
    let patron = /^[a-zA-ZÁÉÍÓÚáéíóúÜü\s]+$/gi;
    if(patron.test(string)){
        return alert('Es un nombre correcto');
    }else{
        return alert('No es un nombre correcto');
    }
    
}

function validarEmail(string){
    let patron = /^([a-zA-Z0-9])+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/i;
    if(patron.test(string)){
        return alert('Es un email correcto');
    }else{
        return alert('No es un email correcto');
    }
    
}
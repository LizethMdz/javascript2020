const $reloj = document.querySelector("#reloj");
const $alarma = document.querySelector("#alarma");
const $btnIniciarR = document.querySelector("#start__reloj");
const $btnDetenerR = document.querySelector("#stop__reloj");
const $btnIniciarA = document.querySelector("#start__alarma");
const $btnDetenerA = document.querySelector("#stop__alarma");
const d = document;

console.log($btnDetenerA);
const crearReloj = function () {
    let fecha = new Date();
    let horas = fecha.getHours();
    let minutos = fecha.getMinutes();
    let segundos = fecha.getSeconds();

    if (horas >= 12 && horas <= 23) {
        return `${horas} : ${minutos} : ${segundos} p.m`;
    } else {
        return `${horas} : ${minutos} : ${segundos} a.m`;
    }


}

let intervalo;

d.addEventListener("click", (e) => {
    if (e.target.matches(".reloj__botones button#start__reloj")) {
        if (e.target.className !== 'disable') {
            $btnIniciarR.classList.toggle('disable');
            $btnIniciarR.setAttribute('disabled', 'true');
            intervalo = setInterval(() => {
                $reloj.textContent = crearReloj();
            }, 1000);
        }
    }

    if (e.target.matches(".reloj__botones button#stop__reloj")) {
        $btnIniciarR.classList.remove('disable');
        $btnIniciarA.setAttribute('disabled', 'false');
        clearInterval(intervalo);
        $reloj.textContent = '';
    }

    if (e.target.matches(".reloj__botones button#start__alarma")) {
        if (e.target.className !== 'disable') {
            $btnIniciarA.classList.toggle('disable');
            $btnIniciarA.setAttribute('disabled', 'true');
            $alarma.play();
        }
    }

    if (e.target.matches(".reloj__botones button#stop__alarma")) {
        $btnIniciarA.classList.remove('disable');
        $btnIniciarA.setAttribute('disabled', 'false');
        $alarma.pause();
    }
});


import hamburgerMenu from "./menu_hamburguesa.js";
import { digitalClock, alarm } from "./reloj.js";
import { shortcuts, moveBall } from "./teclado.js";
import { countDown } from "./countdown.js";
import countdown from "./cuenta_regresiva.js";
import scrollTopBotton from "./boton_scrolll.js";
import darkTheme from "./tema_oscuro.js";

import { responsiveMedia } from "./responsive_design.js";
import { tester } from "./tester.js";
import { userAgent } from "./user_agent.js";
import { statusNetwork } from "./statusNetwork.js";
import { webcam } from "./webcam.js";
import { geolocalization } from "./geolocalization.js";
import { filter } from "./filtro_busqueda.js";
import { sorteoOnline, sorteoOnline_v2 } from "./sorteo_online.js";
import { slider } from "./carrusel.js";
import { scrollSpy } from "./scrollSpy.js";
import { smartVideo } from "./smartVideo.js";
import { contactFormValidations } from "./validaciones.js";
import { narrador } from "./narrador.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  hamburgerMenu(".panel-btn", ".panel", ".menu a");
  digitalClock("#reloj", "#activar-reloj", "#desactivar-reloj");
  alarm("assets/alarma.ogg", "#activar-alarma", "#desactivar-alarma");
  countDown("#countDown", "#startcountdown", "#deadline");
  countdown(
    "countdown",
    "Sep 29 2020 23:18:00",
    "Feliz cumnpleaños Medio Siglo 😔"
  );
  scrollTopBotton(".scroll-top-btn");

  responsiveMedia(
    "youtube",
    "(min-width: 1024px)",
    `<a href="https://www.youtube.com/watch?v=6IwUl-4pAzc&t=1335s" target="_blank" rel="noopener">Ver video</a>`,
    `<iframe width="560" height="315" src="https://www.youtube.com/embed/6IwUl-4pAzc" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen id="youtube"></iframe>`
  );

  responsiveMedia(
    "maps",
    "(min-width: 1024px)",
    `<a href="https://goo.gl/maps/CxEtCvXW8vUZaptd7" target="_blank" rel="noopener">Ver mapa</a>`,
    `<iframe id="maps"
    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.5284824816936!2d-99.15271638526659!3d19.302860686958113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85ce005208010919%3A0x9761ee695190b694!2sEstadio%20Azteca!5e0!3m2!1ses!2smx!4v1599158241177!5m2!1ses!2smx"
    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
    tabindex="0"></iframe>`
  );

  tester("#responsive-test");

  userAgent("#user-device");

  webcam("webcam");

  filter(".card-filter", ".card");

  sorteoOnline("#lenguajes", "#btn-ganador");

  sorteoOnline_v2("#btn-ganador2", ".player");

  slider();

  scrollSpy();

  smartVideo();

  contactFormValidations();


});

d.addEventListener("keydown", (e) => {
  shortcuts(e);
  moveBall(e, ".circle", ".blackBox");
});

// d.addEventListener("keydown", (e) => {
//   shortcut(e);
//   keyboard(e, "#canvas", "#character");
// });

darkTheme(".dark-theme-btn", "dark-mode");

statusNetwork();

geolocalization();

narrador();
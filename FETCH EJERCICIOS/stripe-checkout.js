import STRIPE_KEYS from "./stripe-keys.js";

const d = document,
    $cards = d.getElementById("cards"),
    $template = d.getElementById("card-template").content,
    $fragment = d.createDocumentFragment();


let urls = [
    'https://api.stripe.com/v1/products',
    'https://api.stripe.com/v1/prices'
];

const fetchOptions = {
    headers: {
        Authorization: `Bearer ${STRIPE_KEYS.private}`
    }
}

// map every url to the promise of the fetch
let requests = urls.map(url => fetch(url, fetchOptions));
// Objects
let prices, products;

const moneyFormat = (num) => `$${num.slice(0, -2)}.${num.slice(-2)}`;

Promise.all(requests)
    .then(responses => {
        // all responses are resolved successfully
        for (let response of responses) {
            console.log(`${response.url}: ${response.status}`); // shows 200 for every url
        }

        return responses;
    })
    // map array of responses into an array of response.json() to read their content
    .then(responses => Promise.all(responses.map(r => r.json())))
    // all JSON answers are parsed: "products" is the array of them
    // .then(productos => productos.forEach(producto => console.log(producto.name)));
    .then(json => {
        products = json[0].data;
        prices = json[1].data;

        prices.forEach(el => {
            let productoData = products.filter((product) => product.id === el.product);
            console.log(productoData);
            $template.querySelector(".card").setAttribute("data-price", el.id);
            $template.querySelector("img").src = productoData[0].images[0];
            $template.querySelector("img").alt = productoData[0].name;
            $template.querySelector("figcaption").innerHTML = `${productoData[0].name} 
            <br>
            ${moneyFormat(el.unit_amount_decimal)} ${el.currency}`;
            let $clone = d.importNode($template, true);
            $fragment.appendChild($clone);
        })

        $cards.appendChild($fragment);
    })
    .catch((err) => {
        let message = err.statusText || "Ocurrió un error con el API Stripe";
        $cards.innerHTML = `<p>Error ${err.status} | ${message}</p>`;
    });

// Docs - Github - https://github.com/stripe-samples/checkout-one-time-payments/blob/master/client-only/client/html/index.html
d.addEventListener("click", (e) => {
    console.log(e.target);
    if (e.target.matches(".card *")) {
        let price = e.target.parentElement.getAttribute("data-price");
        Stripe(STRIPE_KEYS.public)
            .redirectToCheckout({
                lineItems: [{ price, quantity: 1 }],
                mode: "subscription",
                successUrl: "http://127.0.0.1:5500/javascript2020/FETCH%20EJERCICIOS/stripe-success.html",
                cancelUrl: "http://127.0.0.1:5500/javascript2020/FETCH%20EJERCICIOS/stripe-cancel.html"
            })
            .then(res => {
                if (res.error) {
                    $cards.insertAdjacentHTML("afterend", res.error.message);
                }
            })
    }
})











// fetch("https://api.stripe.com/v1/products", {
//     headers: {
//         Authorization: `Bearer ${STRIPE_KEYS.private}`
//     }
// })
//     .then((res) => {
//         console.log(res);
//         return res.json();
//     })
//     .then(json => {
//         console.log(json);
//     });


// fetch("https://api.stripe.com/v1/prices", {
//     headers: {
//         Authorization: `Bearer ${STRIPE_KEYS.private}`
//     }
// })
//     .then((res) => {
//         console.log(res);
//         return res.json();
//     })
//     .then(json => {
//         console.log(json);
//     });
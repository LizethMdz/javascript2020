const d = document,
    $estados = d.getElementById("estados"),
    $municipios = d.getElementById("municipios"),
    $colonias = d.getElementById("colonias");


//https://api-sepomex.hckdrk.mx/query/get_estados

let url = "https://api-sepomex.hckdrk.mx/query/get_estados";


function loadedStates() {
    fetch(url)
        .then(response => response.ok ? response.json() : Promise.reject(response))
        .then(json => {
            console.log(json);
            let $opcion = "<option value=''>Elegir estado</option>";
            json.response.estado.forEach(el => {
                $opcion += `<option value="${el}">${el}</option>`;
                $estados.innerHTML = $opcion;
            });
        })
        .catch(err => {
            console.log(err);
            let message = err.statusText || "Ocurrió un error con el API MEX";
            $estados.nextElementSibling.innerHTML = `Error ${err.status} | ${message}`;
        });
}

function loadedTowns(state) {
    fetch(`https://api-sepomex.hckdrk.mx/query/get_municipio_por_estado/${state}`)
        .then(response => response.ok ? response.json() : Promise.reject(response))
        .then(json => {
            console.log(json);
            let $opcion = "<option value=''>Elegir un minicipio</option>";
            json.response.municipios.forEach(el => {
                $opcion += `<option value="${el}">${el}</option>`;
                $municipios.innerHTML = $opcion;
            });
        })
        .catch(err => {
            console.log(err);
            let message = err.statusText || "Ocurrió un error con el API MEX";
            $municipios.nextElementSibling.innerHTML = `Error ${err.status} | ${message}`;
        });
}

function loadedSuburbs(suburb) {
    fetch(`https://api-sepomex.hckdrk.mx/query/get_colonia_por_municipio/${suburb}`)
        .then(response => response.ok ? response.json() : Promise.reject(response))
        .then(json => {
            console.log(json);
            let $opcion = "<option value=''>Elegir una colonia</option>";
            json.response.colonia.forEach(el => {
                $opcion += `<option value="${el}">${el}</option>`;
                $colonias.innerHTML = $opcion;
            });
        })
        .catch(err => {
            console.log(err);
            let message = err.statusText || "Ocurrió un error con el API MEX";
            $colonias.nextElementSibling.innerHTML = `Error ${err.status} | ${message}`;
        });
}


d.addEventListener("DOMContentLoaded", loadedStates);

$estados.addEventListener("change", (e) => loadedTowns(e.target.value));

$municipios.addEventListener("change", (e) => loadedSuburbs(e.target.value));
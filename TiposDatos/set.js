/**Es mas parecido a un arreglo */
const set = new Set([1, 2, 5, 6, 8, true, false, "hola", "HOLA", true]);
console.log(set);

const setConValores = new Set();

setConValores.add(1);
setConValores.add(1);
setConValores.add(6);
setConValores.add({});
setConValores.add("sdsad");

console.log(setConValores);

console.log(setConValores.has(1));

set.forEach(v => {
    console.log(v);
})

console.log('-------------------');


set.forEach(i => {
    console.log(i);
});

console.log('-------------------');

let arr = Array.from(set);
console.log(arr[0]);

console.log(setConValores.delete(1));

console.log(setConValores);

set.entries();
console.log(console);
console.error('Error');
console.warn('Warinig');
console.info('Informacion');
let nombre = 'lizeth';

console.log('Mi nombre es: %s', nombre);

console.clear();

/**Puedo imprimir mi objeto window, document */

console.dir(window);
console.dir(document);

/**Imprimir grupos */

console.group("Cursos@@");
console.log('Java Script');
console.log('Node');
console.groupEnd();

/** Imprimir grupos con tablas */
console.groupCollapsed("Cursos@@");
console.log('Java Script');
console.log('Node');
console.groupEnd();

console.table(Object.entries(console));

/**Timer en el codigo */

console.time('inicio');

for (let index = 0; index < 10; index++) {
    console.log(index);
}

console.timeEnd('inicio');

/**Test o pruebas unitarias */

let x = 13, y = 8;
let msm = 'X DEBE SER MENOR QUE PRUEBA';

console.assert(x < y , {x , y, msm});